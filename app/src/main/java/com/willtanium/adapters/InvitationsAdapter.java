package com.willtanium.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.temple.R;
import com.willtanium.utilities.Loaders;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 6/14/15.
 */
public class InvitationsAdapter extends BaseAdapter implements Filterable {
    private List<TempleBuffers.Invitation> invitations;
    private List<TempleBuffers.Invitation> filteredData;
    private InvitationFilter invitationFilter;

    public InvitationsAdapter(List<TempleBuffers.Invitation> invitations) {
        this.invitations = invitations;
        filteredData = invitations;
        invitationFilter = new InvitationFilter();
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.invitation_item_layout, parent, false);
        }
        TempleBuffers.Invitation invitation = filteredData.get(position);
        InvitationHolder invitationHolder = new InvitationHolder(convertView);
        byte[] data = invitation.getGroup().getGroupLogo().toByteArray();
        invitationHolder.groupImage.setImageBitmap(Loaders.byteImageLoader(data, invitationHolder.groupImage.getWidth(),
                invitationHolder.groupImage.getHeight()));
        convertView.setTag(convertView);
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return invitationFilter;
    }

    private static class InvitationHolder {
        public TextView invitationName;
        public TextView personalMsg;
        public CircleImageView groupImage;

        public InvitationHolder(View view) {
            invitationName = (TextView) view.findViewById(R.id.invitation_name);
            personalMsg = (TextView) view.findViewById(R.id.personal_message);
            groupImage = (CircleImageView) view.findViewById(R.id.group_image);
        }
    }

    private class InvitationFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<TempleBuffers.Invitation> originalInvitations = invitations;
            FilterResults filterResults = new FilterResults();
            int count = originalInvitations.size();
            String filterableString;
            List<TempleBuffers.Invitation> newList = new ArrayList<>(count);
            for (int i = 0; i < count; ++i) {
                TempleBuffers.Invitation invitation = originalInvitations.get(i);
                filterableString = invitation.getGroup().getGroupName();
                if (filterableString.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    newList.add(invitation);
                }
            }
            filterResults.values = newList;
            filterResults.count = newList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<TempleBuffers.Invitation>) results.values;

        }
    }
}
