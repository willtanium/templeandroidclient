package com.willtanium.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.willtanium.storage.models.FriendDB;
import com.willtanium.temple.R;
import com.willtanium.utilities.Loaders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class FriendsAdapter extends BaseAdapter implements Filterable {
    private List<FriendDB> friendDBs;
    private List<FriendDB> filteredData;
    private FriendsFilter friendsFilter;


    public FriendsAdapter(List<FriendDB> friendDBs) {
        this.filteredData = friendDBs;
        this.friendDBs = friendDBs;
        friendsFilter = new FriendsFilter();
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.entity_item_layout, parent, false);
        }
        FriendDB friendDB = filteredData.get(position);
        EntityDataHolder entityDataHolder = new EntityDataHolder(convertView);
        entityDataHolder.entityName.setText(String.format("%s %s", friendDB.getFirstName(), friendDB.getLastName()));
        entityDataHolder.entityImage.setImageBitmap(Loaders.byteImageLoader(friendDB.getPhoto(),
                entityDataHolder.entityImage.getWidth(), entityDataHolder.entityImage.getHeight()));
        convertView.setTag(entityDataHolder);
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return friendsFilter;
    }

    public void replaceData(List<FriendDB> friendDBs) {
        filteredData = friendDBs;
        notifyDataSetChanged();
    }

    private class FriendsFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<FriendDB> originalFriends = friendDBs;
            int count = originalFriends.size();
            FilterResults filterResults = new FilterResults();
            List<FriendDB> newList = new ArrayList<>(count);
            String filterableString;
            for (int i = 0; i < count; ++i) {
                FriendDB friendDB = originalFriends.get(i);
                filterableString = String.format("%s %s", friendDB.getFirstName()
                        , friendDB.getLastName());
                if (filterableString.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    newList.add(friendDB);
                }
            }
            filterResults.values = newList;
            filterResults.count = newList.size();
            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<FriendDB>) results.values;
            notifyDataSetChanged();
        }
    }
}
