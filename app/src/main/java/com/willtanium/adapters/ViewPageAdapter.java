package com.willtanium.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.willtanium.storage.handlers.DatabaseHelper;
import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.handlers.MessageHandlerImpl;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.views.displays.FriendMessageView;
import com.willtanium.views.displays.GroupMessageView;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by william on 7/19/15.
 */
public class ViewPageAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments;
    private MessageHandler messageHandler;
    private GroupMessageView groupMessageView;
    private FriendMessageView friendMessageView;
    public ViewPageAdapter(FragmentManager fm,DatabaseHelper databaseHelper) {
        super(fm);
        try {
            fragments = new LinkedList<>();
            List<FriendDB> friendDBs = databaseHelper.getFriendDao().queryForAll();
            List<GroupDB> groupDBs = databaseHelper.getGroupDao().queryForAll();
            messageHandler = new MessageHandlerImpl(databaseHelper);
            groupMessageView = new GroupMessageView();
            friendMessageView = new FriendMessageView();
            groupMessageView.addGroups(groupDBs,messageHandler);
            friendMessageView.addFriends(friendDBs,messageHandler);
            fragments.add(groupMessageView);
            fragments.add(friendMessageView);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Fragment getItem(int position){
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
