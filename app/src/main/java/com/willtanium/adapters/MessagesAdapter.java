package com.willtanium.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.willtanium.storage.models.MessageDB;
import com.willtanium.storage.models.MessageType;
import com.willtanium.temple.R;
import com.willtanium.utilities.Converters;
import com.willtanium.utilities.Loaders;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 6/14/15.
 */
public class MessagesAdapter extends BaseAdapter{
    private List<MessageDB> messageDBs;
    private byte [] userImage;
    private byte [] senderImage;
    private byte [] data;

    public MessagesAdapter(List<MessageDB> messageDBs, byte [] senderImage, byte [] userImage){
        this.userImage = userImage;
        this.senderImage = senderImage;
        this.messageDBs = messageDBs;
    }

    @Override
    public int getCount() {
        return messageDBs.size();
    }

    @Override
    public Object getItem(int position) {
        return messageDBs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView==null){
            if(messageDBs.get(position).getMessageType().equals(MessageType.RECEIVED)){
                data = senderImage;
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.received_message_layout,parent, false);
            }else{
                data = userImage;
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sent_message_layout, parent, false);
            }
            MessageHolder holder = new MessageHolder(convertView);
            holder.message.setText(messageDBs.get(position).getMessage());
            holder.timeStamp.setText(Converters.dateToString(messageDBs.get(position).getTimeStamp()));
            holder.senderImage.setImageBitmap(Loaders.byteImageLoader(data,
                    holder.senderImage.getWidth(),
                    holder.senderImage.getHeight()));
        }
        return convertView;
    }

    public void addMessage(MessageDB messageDB){
        messageDBs.add(messageDB);
        notifyDataSetChanged();
    }

    public static class MessageHolder{
        protected TextView message;
        protected TextView timeStamp;
        protected CircleImageView senderImage;
        public MessageHolder(View view){
               message = (TextView)view.findViewById(R.id.message);
               timeStamp = (TextView)view.findViewById(R.id.time_stamp);
               senderImage = (CircleImageView)view.findViewById(R.id.sender_image);
        }
    }
}
