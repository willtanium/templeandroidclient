package com.willtanium.adapters;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.willtanium.storage.models.FriendDB;
import com.willtanium.temple.R;
import com.willtanium.utilities.Loaders;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 7/22/15.
 */
public class FriendSelectAdapter extends BaseAdapter implements Filterable {
    private List<FriendDB> friendDBs;
    private List<FriendDB> filteredData;
    private FilteredFriends filteredFriends;
    private SparseBooleanArray choiceArray;


    public FriendSelectAdapter(List<FriendDB> friendDBs) {
        this.filteredData = friendDBs;
        this.friendDBs = friendDBs;
        filteredFriends = new FilteredFriends();
        choiceArray = new SparseBooleanArray(friendDBs.size());
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    public List<FriendDB> getChoiceArray() {
        List<FriendDB> chosen = new ArrayList<>();
        int count = 0;

        for (int i = 0; i < filteredData.size(); ++i) {
            if (choiceArray.get(count)) {
                chosen.add(friendDBs.get(count));
            }
        }
        return chosen;
    }


    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.friend_select_item, parent, false);
        }
        SelectFriendHolder selectFriendHolder = new SelectFriendHolder(convertView);
        FriendDB friendDB = filteredData.get(position);
        selectFriendHolder.name.setText(String.format("%s %s", friendDB.getFirstName(),
                friendDB.getLastName()));
        selectFriendHolder.profile.setImageBitmap(Loaders.byteImageLoader(friendDB.getPhoto(),
                selectFriendHolder.profile.getWidth(), selectFriendHolder.profile.getHeight()));
        if (selectFriendHolder.selected.isChecked()) {
            choiceArray.put(position, selectFriendHolder.selected.isChecked());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return filteredFriends;
    }

    private class SelectFriendHolder {
        public TextView name;
        public CircleImageView profile;
        public CheckBox selected;

        public SelectFriendHolder(View view) {
            name = (TextView) view.findViewById(R.id.entity_name);
            profile = (CircleImageView) view.findViewById(R.id.profile_image);
            selected = (CheckBox) view.findViewById(R.id.select);
        }
    }

    private class FilteredFriends extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<FriendDB> originalData = friendDBs;
            int count = originalData.size();
            List<FriendDB> newList = new ArrayList<>();
            String filterableString;
            FilterResults filterResults = new FilterResults();
            for (int i = 0; i < count; ++i) {
                FriendDB friendDB = originalData.get(i);
                filterableString = String.format("%s %s", friendDB.getFirstName(),
                        friendDB.getLastName());
                if (filterableString.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    newList.add(friendDB);
                }
            }

            filterResults.values = newList;
            filterResults.count = newList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<FriendDB>) results.values;
            choiceArray = new SparseBooleanArray(results.count);
            notifyDataSetChanged();
        }
    }
}
