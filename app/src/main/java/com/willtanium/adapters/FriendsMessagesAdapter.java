package com.willtanium.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.temple.R;
import com.willtanium.utilities.Converters;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 7/18/15.
 */
public class FriendsMessagesAdapter extends BaseAdapter implements Filterable {
    private List<FriendDB> friendDBs;
    private List<FriendDB> filteredData;
    private MessageHandler messageHandler;
    private FriendsFilter friendsFilter;
    public FriendsMessagesAdapter(List<FriendDB> friendDBs, MessageHandler messageHandler){
        this.friendDBs = friendDBs;
        this.filteredData = friendDBs;
        friendsFilter = new FriendsFilter();
        this.messageHandler = messageHandler;
    }
    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_preview_layout, parent, false);
        }
            FriendsPreviewHolder friendsPreviewHolder = new FriendsPreviewHolder(convertView);
        FriendDB friendDB = filteredData.get(position);
            MessageDB messageDB = getLastMessage(friendDB.getFriendKey());
            friendsPreviewHolder.friendsImage.setImageBitmap(Converters.getBitmapFromBytes(friendDB.getPhoto()));
            friendsPreviewHolder.timeStamp.setText(Converters.dateToString(messageDB.getTimeStamp()));
            friendsPreviewHolder.lastMessage.setText(messageDB.getMessage());


        return convertView;
    }
    private MessageDB getLastMessage(String targetID){
        return messageHandler.getLastMessageBy(targetID);
    }

    @Override
    public Filter getFilter() {
        return friendsFilter;
    }

    protected static class FriendsPreviewHolder{
        protected TextView lastMessage;
        protected CircleImageView friendsImage;
        protected TextView timeStamp;
        public FriendsPreviewHolder(View view){
            lastMessage = (TextView)view.findViewById(R.id.last_message);
            friendsImage = (CircleImageView)view.findViewById(R.id.profile_image);
            timeStamp = (TextView)view.findViewById(R.id.time_stamp);
        }
    }

    private class FriendsFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<FriendDB> originalFriends = friendDBs;
            int count = originalFriends.size();
            FilterResults filterResults = new FilterResults();
            List<FriendDB> newList = new ArrayList<>(count);
            String filterableString;
            for (int i = 0; i < count; ++i) {
                FriendDB friendDB = originalFriends.get(i);
                filterableString = String.format("%s %s", friendDB.getFirstName()
                        , friendDB.getLastName());
                if (filterableString.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    newList.add(friendDB);
                }
            }
            filterResults.values = newList;
            filterResults.count = newList.size();
            return filterResults;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<FriendDB>) results.values;
            notifyDataSetChanged();
        }
    }
}
