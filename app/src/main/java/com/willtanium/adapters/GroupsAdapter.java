package com.willtanium.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import com.willtanium.storage.models.GroupDB;
import com.willtanium.temple.R;
import com.willtanium.utilities.Loaders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class GroupsAdapter extends BaseAdapter implements Filterable {
    private List<GroupDB> groupDBs;
    private List<GroupDB> filteredData;
    private GroupFilter groupFilter = new GroupFilter();

    public GroupsAdapter(List<GroupDB> groupDBs) {
        this.groupDBs = groupDBs;
        this.filteredData = groupDBs;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.entity_item_layout, parent, false);
        }
        GroupDB groupDB = filteredData.get(position);
        EntityDataHolder entityDataHolder = new EntityDataHolder(convertView);
        entityDataHolder.entityName.setText(groupDB.getGroupName());
        entityDataHolder.entityImage.setImageBitmap(Loaders.byteImageLoader(groupDB.getGroupPhoto(),
                entityDataHolder.entityImage.getWidth(), entityDataHolder.entityImage.getHeight()));
        convertView.setTag(entityDataHolder);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return groupFilter;
    }

    private class GroupFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<GroupDB> originalGroups = groupDBs;
            int count = originalGroups.size();
            FilterResults filterResults = new FilterResults();
            List<GroupDB> newList = new ArrayList<>(count);
            String filterableString;
            for (int i = 0; i < count; ++i) {
                GroupDB groupDB = originalGroups.get(i);
                filterableString = groupDB.getGroupName();
                if (filterableString.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    newList.add(groupDB);
                }
            }
            filterResults.values = newList;
            filterResults.count = newList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<GroupDB>) results.values;
            notifyDataSetChanged();
        }
    }

}
