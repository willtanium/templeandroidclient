package com.willtanium.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.temple.R;
import com.willtanium.utilities.Converters;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 7/18/15.
 */
public class GroupsMessagesAdapter extends BaseAdapter implements Filterable {
    private List<GroupDB> groupDBs;
    private List<GroupDB> filteredData;
    private GroupFilter groupFilter;
    private MessageHandler messageHandler;


    public GroupsMessagesAdapter(List<GroupDB> groupDBs, MessageHandler messageHandler){
        this.groupDBs = groupDBs;
        this.messageHandler = messageHandler;
        filteredData = groupDBs;
        groupFilter = new GroupFilter();
    }
    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_preview_layout, parent, false);
        }
            GroupPreviewHolder groupPreviewHolder = new GroupPreviewHolder(convertView);
        GroupDB groupDB = filteredData.get(position);
            MessageDB messageDB = getLastMessage(groupDB.getGroupKey());
            groupPreviewHolder.lastMessage.setText(messageDB.getMessage());
            groupPreviewHolder.groupImage.setImageBitmap(
                    Converters.getBitmapFromBytes(groupDB.getGroupPhoto()));
            groupPreviewHolder.timeStamp.setText(Converters.dateToString(messageDB.getTimeStamp()));


        return convertView;
    }

    private MessageDB getLastMessage(String targetID){
       return messageHandler.getLastMessageBy(targetID);
    }

    @Override
    public Filter getFilter() {
        return groupFilter;
    }


    protected static class GroupPreviewHolder{
        protected TextView lastMessage;
        protected CircleImageView groupImage;
        protected TextView timeStamp;
        public GroupPreviewHolder(View view){
            lastMessage = (TextView)view.findViewById(R.id.last_message);
            groupImage = (CircleImageView)view.findViewById(R.id.profile_image);
            timeStamp = (TextView)view.findViewById(R.id.time_stamp);
        }
    }

    private class GroupFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            final List<GroupDB> originalGroups = groupDBs;
            int count = originalGroups.size();
            FilterResults filterResults = new FilterResults();
            List<GroupDB> newList = new ArrayList<>(count);
            String filterableString;
            for (int i = 0; i < count; ++i) {
                GroupDB groupDB = originalGroups.get(i);
                filterableString = groupDB.getGroupName();
                if (filterableString.toLowerCase().contains(constraint.toString().toLowerCase())) {
                    newList.add(groupDB);
                }
            }
            filterResults.values = newList;
            filterResults.count = newList.size();
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (List<GroupDB>) results.values;
            notifyDataSetChanged();
        }
    }
}
