package com.willtanium.adapters;

import android.view.View;
import android.widget.TextView;

import com.willtanium.temple.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 7/21/15.
 */
public class EntityDataHolder {
    public TextView entityName;
    public CircleImageView entityImage;

    public EntityDataHolder(View view) {
        entityImage = (CircleImageView) view.findViewById(R.id.profile_image);
        entityName = (TextView) view.findViewById(R.id.entity_name);
    }
}
