package com.willtanium.storage.models;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by william on 6/14/15.
 */

@DatabaseTable(tableName = "ChatMessages")
public class MessageDB implements Serializable{

    public MessageDB(String message, Date timeStamp, String senderID, String receiverID, MessageType messageType) {
        this.message = message;
        this.timeStamp = timeStamp;
        this.senderID = senderID;
        this.receiverID = receiverID;
        this.messageType = messageType;
    }

    public MessageDB() {
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public ForeignCollection<AttachmentDB> getAttachmentDBs() {
        return attachmentDBs;
    }

    public String getSenderID() {
        return senderID;
    }

    public String getReceiverID() {
        return receiverID;
    }

    @DatabaseField(id = true)
    private int id;
    @DatabaseField(columnName = "message", dataType = DataType.STRING)
    private String message;
    @DatabaseField(columnName = "time_stamp", dataType = DataType.DATE)
    private Date timeStamp;
    @ForeignCollectionField(eager = true, foreignColumnName = "attachments")
    private ForeignCollection<AttachmentDB> attachmentDBs;
    @DatabaseField(columnName = "sender_key", dataType = DataType.STRING)
    private String senderID;
    @DatabaseField(columnName = "receiver_key", dataType = DataType.STRING)
    private String receiverID;

    public MessageType getMessageType() {
        return messageType;
    }

    @DatabaseField(columnName = "message_type", dataType = DataType.ENUM_STRING)
    private MessageType messageType;



    @Override
    public boolean equals(Object object){
        if( object instanceof MessageDB){
            if(((MessageDB)object).getSenderID().equals(this.senderID)){
                return true;
            }
        }
        return false;
    }

}
