package com.willtanium.storage.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by william on 6/14/15.
 */

@DatabaseTable(tableName = "Group")
public class GroupDB implements Serializable{

    public GroupDB(String groupName, String groupDescription, String groupKey, byte[] groupPhoto) {
        this.groupName = groupName;
        this.groupDescription = groupDescription;
        this.groupKey = groupKey;
        this.groupPhoto = groupPhoto;
    }

    @DatabaseField(id=true)
    private int id;

    public GroupDB() {
    }

    public int getId() {
        return id;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public byte[] getGroupPhoto() {
        return groupPhoto;
    }

    @DatabaseField(columnName = "group_name", dataType = DataType.STRING)
    private String groupName;
    @DatabaseField(columnName = "group_description", dataType = DataType.STRING)
    private String groupDescription;
    @DatabaseField(columnName = "group_key", dataType = DataType.STRING)
    private String groupKey;
    @DatabaseField(columnName = "group_photo", dataType = DataType.BYTE_ARRAY)
    private byte [] groupPhoto;
}
