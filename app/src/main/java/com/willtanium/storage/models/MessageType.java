package com.willtanium.storage.models;

import java.io.Serializable;

/**
 * Created by william on 6/15/15.
 */
public enum MessageType implements Serializable{
    SENT("SENT"), RECEIVED("RECEIVED");

    private String messageType;

    private MessageType(String messageType){
        this.messageType = messageType;
    }
    @Override
    public String toString(){
        return messageType;
    }
}
