package com.willtanium.storage.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by william on 6/14/15.
 */


@DatabaseTable(tableName = "Friends")
public class FriendDB implements Serializable{
    @DatabaseField(id = true)
    private int id;



    public FriendDB(String firstName, String lastName, String phoneNumber, String emailAddress, byte[] photo, String friendKey) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
        this.photo = photo;
        this.friendKey = friendKey;
    }

    public FriendDB() {
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public byte[] getPhoto() {
        return photo;
    }

    @DatabaseField(columnName = "first_name",dataType = DataType.STRING)
    private String firstName;
    @DatabaseField(columnName = "last_name",dataType = DataType.STRING)
    private String lastName;
    @DatabaseField(columnName = "phone_number",dataType = DataType.STRING)
    private String phoneNumber;
    @DatabaseField(columnName = "email_address",dataType = DataType.STRING)
    private String emailAddress;
    @DatabaseField(columnName = "friend_photo",dataType = DataType.BYTE_ARRAY)
    private byte [] photo;

    public String getFriendKey() {
        return friendKey;
    }

    @DatabaseField(columnName = "friend_key",dataType = DataType.STRING)
    private String friendKey;

}
