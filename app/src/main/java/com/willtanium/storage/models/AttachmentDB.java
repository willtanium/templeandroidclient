package com.willtanium.storage.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by william on 6/14/15.
 */

@DatabaseTable(tableName = "attachments")
public class AttachmentDB implements Serializable{

    @DatabaseField(id = true)
    private int id;

    public AttachmentDB() {
    }



    public int getId() {
        return id;
    }

    public String getFileURI() {
        return fileURI;
    }

    public String getFilename() {
        return filename;
    }

    public int getFileSize() {
        return fileSize;
    }

    public AttachmentDB(String fileURI, String filename, int fileSize, MessageDB messageDB) {
        this.fileURI = fileURI;
        this.filename = filename;
        this.fileSize = fileSize;
        this.messageDB = messageDB;
    }

    @DatabaseField(columnName = "file_uri", dataType = DataType.STRING)
    private String fileURI;
    @DatabaseField(columnName = "file_name", dataType = DataType.STRING)
    private String filename;
    @DatabaseField(columnName = "size", dataType = DataType.INTEGER)
    private int fileSize;

    public MessageDB getMessageDB() {
        return messageDB;
    }

    @DatabaseField(foreignColumnName = "root_message", foreign = true)
    private MessageDB messageDB;

}
