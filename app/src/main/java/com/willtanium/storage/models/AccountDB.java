package com.willtanium.storage.models;

import java.io.Serializable;

/**
 * Created by william on 6/14/15.
 */
public class AccountDB implements Serializable{
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getAccountKey() {
        return accountKey;
    }

    private String firstName;
    private String lastName;
    private String emailAddress;
    private String phoneNumber;
    private byte [] photo;
    private String accountKey;

    public AccountDB(String firstName, String lastName, String emailAddress, String phoneNumber, byte[] photo, String accountKey) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
        this.photo = photo;
        this.accountKey = accountKey;
    }

    public boolean isInitialized(){
        if(!firstName.isEmpty() && !lastName.isEmpty() && !emailAddress.isEmpty() && !phoneNumber.isEmpty()){
            if(photo.length > 0 || !accountKey.isEmpty()){
                return true;
            }
        }
        return false;
    }
    public boolean copy(AccountDB  accountDB){
        if(accountDB.isInitialized()){
            this.firstName = accountDB.getFirstName();
            this.lastName = accountDB.getLastName();
            this.emailAddress = accountDB.getEmailAddress();
            this.phoneNumber = accountDB.getPhoneNumber();
            if(accountDB.getPhoto().length > 0){
                this.photo = accountDB.getPhoto();
            }
            if(!accountDB.getAccountKey().isEmpty()){
                this.accountKey = accountDB.getAccountKey();
            }
            return true;
        }
        return false;
    }
}
