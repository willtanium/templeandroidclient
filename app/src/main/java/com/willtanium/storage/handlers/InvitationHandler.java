package com.willtanium.storage.handlers;

import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.GroupDB;

import java.util.List;

/**
 * Created by william on 6/15/15.
 */
public interface InvitationHandler {
    boolean addInvitation(TempleBuffers.Invitation invitation);
    boolean removeInvitation(TempleBuffers.Invitation invitation);
    List<TempleBuffers.Invitation> getInvitations();
    TempleBuffers.InvitationOut createInvitation(GroupDB groupDB,String personalMessage, List<String> phoneNumbers);
    boolean acceptInvitation(DatabaseHelper dbHelper, TempleBuffers.Invitation invitation);
}
