package com.willtanium.storage.handlers;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.GroupDB;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class GroupHandlerImpl implements GroupHandler {
    private DatabaseHelper databaseHelper;

    public GroupHandlerImpl(DatabaseHelper databaseHelper){
        this.databaseHelper = databaseHelper;
    }

    @Override
    public boolean saveGroup(TempleBuffers.Group group) {
        GroupDB groupDB = new GroupDB(group.getGroupName(),
                group.getGroupDescription()
                ,group.getGroupKey(),
                group.getGroupLogo().toByteArray());
        try {
            databaseHelper.getGroupDao().createIfNotExists(groupDB);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public GroupDB getGroup(String groupKey){
        QueryBuilder<GroupDB,Integer> queryBuilder = databaseHelper.getGroupDao().queryBuilder();
        try {
            queryBuilder.where().eq("group_key",groupKey);
            return queryBuilder.queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean removeGroup(String groupKey){
        DeleteBuilder<GroupDB, Integer> deleteBuilder = databaseHelper.getGroupDao().deleteBuilder();
        try {
            deleteBuilder.where().eq("group_key", groupKey);
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<GroupDB> getAllGroups(){
        try {
            return databaseHelper.getGroupDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
