package com.willtanium.storage.handlers;

import com.willtanium.protobuf.TempleBuffers.Group;
import com.willtanium.storage.models.GroupDB;

import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public interface GroupHandler {
    boolean saveGroup(Group group);
    GroupDB getGroup(String groupKey);
    boolean removeGroup(String groupKey);
    List<GroupDB> getAllGroups();
}
