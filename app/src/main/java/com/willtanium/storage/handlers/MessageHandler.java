package com.willtanium.storage.handlers;

import com.willtanium.protobuf.TempleBuffers.ChatMessage;
import com.willtanium.storage.models.MessageDB;

import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public interface MessageHandler {
    MessageDB saveMessage(ChatMessage chatMessage);
    boolean saveMessage(String messageData, String receiverID);
    boolean removeMessage(int messageID);
    boolean removeConversation(String targetID);
    List<MessageDB> getAllFirstMessages();
    List<MessageDB> getConversation(String targetID);
    MessageDB getLastMessageBy(String targetID);
}
