package com.willtanium.storage.handlers;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.storage.models.AttachmentDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.storage.models.MessageType;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class MessageHandlerImpl implements MessageHandler{
    private DatabaseHelper databaseHelper;
    private AccountDB user;
    public MessageHandlerImpl(DatabaseHelper databaseHelper){
        this.databaseHelper = databaseHelper;
    }

    @Override
    public MessageDB saveMessage(TempleBuffers.ChatMessage chatMessage) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yy HH:mm");

        try {
            MessageDB messageDB = new MessageDB(chatMessage.getMessageData(),
                                                simpleDateFormat.parse(chatMessage.getTimeStamp()),
                                                chatMessage.getSenderId(),
                                                user.getAccountKey(),MessageType.RECEIVED);
            messageDB = databaseHelper.getMessageDao().createIfNotExists(messageDB);
            for (TempleBuffers.Attachment attachment : chatMessage.getAttachmentsList()){
                AttachmentDB attachmentDB = new AttachmentDB(attachment.getAttachmentKey(),
                        attachment.getAttachmentKey(), attachment.getFileSize(), messageDB);
                databaseHelper.getAttachmentDao().createIfNotExists(attachmentDB);
            }
            return databaseHelper.getMessageDao().queryForId(messageDB.getId());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean saveMessage(String messageData, String receiverID) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yy HH:mm");
        try {
            MessageDB messageDB = new MessageDB(messageData,
                                                simpleDateFormat.parse(new Date().toString()),
                                                user.getAccountKey(),receiverID, MessageType.SENT);
            databaseHelper.getMessageDao().createIfNotExists(messageDB);
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean removeMessage(int messageID) {
        DeleteBuilder<MessageDB,Integer> deleteBuilder = databaseHelper.getMessageDao().deleteBuilder();
        try {
            deleteBuilder.where().eq("id", messageID);
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean removeConversation(String targetID){
        DeleteBuilder<MessageDB,Integer> deleteBuilder = databaseHelper.getMessageDao().deleteBuilder();
        try {
            deleteBuilder.where().eq("sender_key",targetID).and().
                    eq("receiver_key",user.getAccountKey()).or().eq("sender_key",user.getAccountKey()).and()
                    .eq("receiver_key", targetID);
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<MessageDB> getAllFirstMessages(){
        List<MessageDB> messages = new LinkedList<>();
        QueryBuilder<MessageDB,Integer> queryBuilder = databaseHelper.getMessageDao().queryBuilder();
        try {
            queryBuilder.where().ne("sender_key", user.getAccountKey());
            queryBuilder.orderBy("time_stamp", false);
            List<MessageDB> msgs = queryBuilder.query();
            for(MessageDB messageDB : msgs){
                if(messages.contains(messageDB)){ continue;}
                messages.add(messageDB);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }
    @Override
    public MessageDB getLastMessageBy(String targetID){
        QueryBuilder<MessageDB, Integer> queryBuilder = databaseHelper.getMessageDao().queryBuilder();
        try {
            queryBuilder.where().eq("sender_key", targetID).or().eq("receiver_key",targetID);
            queryBuilder.orderBy("time_stamp",false);
            return queryBuilder.queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    

    @Override
    public List<MessageDB> getConversation(String targetID) {
        QueryBuilder<MessageDB,Integer> queryBuilder = databaseHelper.getMessageDao().queryBuilder();
        try {
            queryBuilder.where().eq("sender_key",targetID).and().eq("receiver_key",user.getAccountKey())
                    .or().eq("sender_key",user.getAccountKey()).and().eq("receiver_key",targetID);
            queryBuilder.orderBy("time_stamp",false);
            return queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
