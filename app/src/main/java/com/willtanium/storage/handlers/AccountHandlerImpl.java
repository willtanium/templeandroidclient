package com.willtanium.storage.handlers;

import com.willtanium.storage.models.AccountDB;
import com.willtanium.utilities.FileHandler;

/**
 * Created by william on 6/15/15.
 */
public class AccountHandlerImpl implements AccountHandler{
    private static AccountHandler accountInstance;
    private static final String ACCOUNT_PATH ="temple.ss";
    private FileHandler fileHandler;


    private AccountHandlerImpl(){
        fileHandler = new FileHandler(ACCOUNT_PATH);

    }
    @Override
    public boolean saveUser(AccountDB accountDB){
        if(accountDB != null){
            if(accountDB.isInitialized()){
                fileHandler.saveObject(accountDB);
                return true;
            }
        }
        return false;
    }

    @Override
    public AccountDB getUser(){
        if(fileHandler.isOpen()){
            return (AccountDB)fileHandler.getObject();
        }
        return null;
    }

    @Override
    public boolean verifyDetails(AccountDB accountDB){
        if(accountDB != null){
            return accountDB.isInitialized();
        }
        return false;
    }

    @Override
    public boolean updateUser(AccountDB accountDB){
        if(accountDB != null) {
            AccountDB account = getUser();
            account.copy(accountDB);
            return saveUser(account);
        }
        return false;
    }
    public static AccountHandler getAccountHandler(){
        if(accountInstance == null){
            accountInstance = new AccountHandlerImpl();
        }
        return accountInstance;
    }
}
