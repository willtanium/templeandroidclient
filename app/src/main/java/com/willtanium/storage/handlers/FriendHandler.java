package com.willtanium.storage.handlers;

import com.willtanium.protobuf.TempleBuffers.Friend;
import com.willtanium.storage.models.FriendDB;

import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public interface FriendHandler {
    boolean saveFriend(Friend friend);
    FriendDB getFriend(String friendKey);
    boolean removeFriend(String friendKey);
    List<FriendDB> getAllFriends();

}
