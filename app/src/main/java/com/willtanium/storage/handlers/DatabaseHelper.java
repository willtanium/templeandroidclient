package com.willtanium.storage.handlers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.willtanium.storage.models.AttachmentDB;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.storage.models.MessageDB;

import java.sql.SQLException;

/**
 * Created by william on 6/14/15.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "temple.db";
    private static final int DATABASE_VERSION = 1;
    private Dao<MessageDB, Integer> messageDao;
    private Dao<AttachmentDB, Integer> attachmentDao;
    private Dao<FriendDB, Integer> friendDao;
    private Dao<GroupDB, Integer> groupDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource,FriendDB.class);
            TableUtils.createTable(connectionSource,GroupDB.class);
            TableUtils.createTable(connectionSource,MessageDB.class);
            TableUtils.createTable(connectionSource,AttachmentDB.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, FriendDB.class,false);
            TableUtils.dropTable(connectionSource, GroupDB.class, false);
            TableUtils.dropTable(connectionSource, MessageDB.class, false);
            TableUtils.dropTable(connectionSource, AttachmentDB.class, false);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Dao<FriendDB, Integer> getFriendDao(){
        if(friendDao==null){
            try {
                friendDao = getDao(FriendDB.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return friendDao;
    }
    public Dao<GroupDB, Integer> getGroupDao(){
        if(groupDao==null){
            try {
                groupDao = getDao(GroupDB.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return groupDao;
    }
    public Dao<MessageDB, Integer> getMessageDao(){
        if(messageDao==null){
            try {
                messageDao = getDao(MessageDB.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return messageDao;
    }

    public Dao<AttachmentDB, Integer> getAttachmentDao(){
        if(attachmentDao==null){
            try {
                attachmentDao = getDao(AttachmentDB.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return attachmentDao;
    }

}
