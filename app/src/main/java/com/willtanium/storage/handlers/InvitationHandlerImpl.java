package com.willtanium.storage.handlers;

import com.google.protobuf.InvalidProtocolBufferException;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.utilities.FileHandler;

import java.util.List;

/**
 * Created by william on 6/15/15.
 */
public class InvitationHandlerImpl implements InvitationHandler{
    private FileHandler fileHandler;



    private List<TempleBuffers.Invitation> invitations;

    private static final String INVITATIONS_DB = "invitations.inv";
    public InvitationHandlerImpl(){
        fileHandler = new FileHandler(INVITATIONS_DB);
        try {
            invitations = TempleBuffers.InvitationPack
                    .parseFrom(fileHandler.readDataBytes()).getInvitationsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }



    @Override
    public boolean addInvitation(TempleBuffers.Invitation invitation){

            invitations.add(invitation);
            TempleBuffers.InvitationPack.Builder invPkBuilder = TempleBuffers.InvitationPack.newBuilder();
            invPkBuilder.addInvitations(invitation);
            fileHandler.storeData(invPkBuilder.build().toByteArray());

        return true;
    }

    @Override
    public boolean removeInvitation(TempleBuffers.Invitation invitation){
        int counter = 0;
        for(TempleBuffers.Invitation inv: invitations){
            if(inv.getGroup().getGroupKey().equals(invitation.getGroup().getGroupKey())){
                TempleBuffers.InvitationPack.Builder invPkBuilder = TempleBuffers.InvitationPack.newBuilder();
                invitations.remove(counter);
                invPkBuilder.addAllInvitations(invitations);
                fileHandler.storeData(invPkBuilder.build().toByteArray());
                return true;
            }
            counter++;
        }
        return false;
    }

    @Override
    public List<TempleBuffers.Invitation> getInvitations() {

        try {
            return TempleBuffers.InvitationPack.parseFrom(fileHandler.readDataBytes()).getInvitationsList();
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public TempleBuffers.InvitationOut createInvitation(GroupDB groupDB, String personalMessage, List<String> phoneNumbers){

        TempleBuffers.InvitationOut.Builder inviBuilder = TempleBuffers.InvitationOut.newBuilder();
        inviBuilder.setGroupId(groupDB.getGroupKey());
        inviBuilder.setPersonalMessage(personalMessage);
        inviBuilder.addAllContacts(phoneNumbers);
        return inviBuilder.build();
    }

    @Override
    public boolean acceptInvitation(DatabaseHelper dbHelper, TempleBuffers.Invitation invitation) {
        if(dbHelper != null && invitation != null){
            GroupHandler groupHandler = new GroupHandlerImpl(dbHelper);
            groupHandler.saveGroup(invitation.getGroup());
            removeInvitation(invitation);
            return true;
        }
        return false;
    }
}
