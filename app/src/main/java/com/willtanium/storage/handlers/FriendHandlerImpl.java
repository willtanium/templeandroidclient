package com.willtanium.storage.handlers;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.FriendDB;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class FriendHandlerImpl implements FriendHandler {
    private DatabaseHelper databaseHelper;

    public FriendHandlerImpl(DatabaseHelper databaseHelper){
        this.databaseHelper = databaseHelper;
    }

    @Override
    public boolean saveFriend(TempleBuffers.Friend friend){
        FriendDB friendDB = new FriendDB(friend.getFirstName(),
                                         friend.getLastName(),
                                         friend.getPhoneNumber(),
                                         friend.getEmailAddress(),
                                         friend.getPhotoData().toByteArray(),
                                         friend.getFriendKey());
        try {
            databaseHelper.getFriendDao().createIfNotExists(friendDB);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public FriendDB getFriend(String friendKey){
        try {
            QueryBuilder<FriendDB,Integer> queryBuilder = databaseHelper.getFriendDao().queryBuilder();
            queryBuilder.where().eq("friend_key", friendKey);
            return queryBuilder.queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean removeFriend(String friendKey){
        DeleteBuilder<FriendDB, Integer> deleteBuilder = databaseHelper.getFriendDao().deleteBuilder();
        try {
            deleteBuilder.where().eq("friend_key", friendKey);
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<FriendDB> getAllFriends(){
        try {
            return databaseHelper.getFriendDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
