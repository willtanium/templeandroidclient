package com.willtanium.storage.handlers;

import com.willtanium.storage.models.AccountDB;

/**
 * Created by william on 6/15/15.
 */
public interface AccountHandler {
    boolean saveUser(AccountDB accountDB);
    AccountDB getUser();
    boolean verifyDetails(AccountDB accountDB);
    boolean updateUser(AccountDB accountDB);
}
