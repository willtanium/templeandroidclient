package com.willtanium.services;

import com.willtanium.protobuf.TempleBuffers;

/**
 * Created by william on 7/17/15.
 */
public interface OnRegisteredListener {
    void onRegistered(TempleBuffers.UserData userData);
    void onRegistrationFailed(String message);
}
