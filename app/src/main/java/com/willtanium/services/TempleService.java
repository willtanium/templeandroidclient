package com.willtanium.services;

import com.squareup.okhttp.Response;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.protobuf.TempleBuffers.UserReg;
import com.willtanium.protobuf.TempleBuffers.UserData;


import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;

/**
 * Created by william on 6/14/15.
 */
public interface TempleService {
    @POST("/friend_registration")
    void registerUser(@Body UserReg userReg, Callback<UserData> userData);
    @POST("/group_registration")
    void registerGroup(@Body TempleBuffers.GroupReg groupReg, Callback<TempleBuffers.Group> group);
    @POST("/friend_search")
    void searchForFriends(@Body TempleBuffers.SearchQuery searchQuery,
                          Callback<TempleBuffers.SearchResponse> response);
    @POST("/invitation")
    void sendInvitation(TempleBuffers.InvitationOut invitationOut, Callback<String> response);

    @Multipart
    @POST("/file_upload")
    void fileUpload(@Part("fileContent")TypedFile file, Callback<TempleBuffers.Attachment> attachment);
    @GET("file_request/{file_name}")
    Response getFile(@Path("file_name") String fileName);




}
