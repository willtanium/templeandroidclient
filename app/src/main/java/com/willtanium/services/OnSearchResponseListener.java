package com.willtanium.services;

import com.willtanium.protobuf.TempleBuffers;

/**
 * Created by william on 7/17/15.
 */
public interface OnSearchResponseListener{
    void onFriendsFound(TempleBuffers.SearchResponse searchResponse);
    void onSearchFailed(String message);
}
