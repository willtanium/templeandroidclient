package com.willtanium.services;

import com.willtanium.protobuf.TempleBuffers;

/**
 * Created by william on 7/17/15.
 */
public interface OnGroupRegisteredListener {
    void onGroupRegistered(TempleBuffers.Group group);
    void onGroupRegistrationFailed(String message);
}
