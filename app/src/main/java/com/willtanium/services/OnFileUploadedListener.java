package com.willtanium.services;

import com.willtanium.protobuf.TempleBuffers;

/**
 * Created by william on 7/17/15.
 */
public interface OnFileUploadedListener{
    void onFileUploaded(TempleBuffers.Attachment attachment);
    void onFileUploadFailed(String message);
}
