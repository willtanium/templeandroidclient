package com.willtanium.services;

import android.content.Context;
import android.util.Log;

import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.protobuf.TempleBuffers.UserData;
import com.willtanium.protobuf.TempleBuffers.UserReg;
import com.willtanium.temple.R;
import com.willtanium.utilities.FileHandler;

import java.io.File;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.ProtoConverter;
import retrofit.mime.TypedFile;

/**
 * Created by william on 7/17/15.
 */
public class TempleServiceHandler {
    private TempleService templeService;
    private RestAdapter restAdapter;
    private OnRegisteredListener registrationListener;
    private OnInvitationSentListener invitationSentListener;
    private OnGroupRegisteredListener groupRegisteredListener;
    private OnSearchResponseListener searchResponseListener;
    private OnFileUploadedListener fileUploadedListener;

    public TempleServiceHandler(Context context){
        String endPoint = context.getResources().getString(R.string.end_point);
            restAdapter = new RestAdapter.Builder()
                    .setEndpoint(endPoint)
                    .setConverter(new ProtoConverter())
                    .setLogLevel(RestAdapter.LogLevel.FULL).build();
        templeService = restAdapter.create(TempleService.class);

    }



    public void registerUser(UserReg userReg){
        templeService.registerUser(userReg, new UserDataCallback());
    }

    public void setRegistrationListener(OnRegisteredListener registrationListener){
        this.registrationListener = registrationListener;
    }
    public void sendInvitation(TempleBuffers.InvitationOut invitationOut){
        templeService.sendInvitation(invitationOut, new InvitationCallback());
    }
    public void setInvitationSentListener(OnInvitationSentListener invitationSentListener){
        this.invitationSentListener = invitationSentListener;
    }

    public void registerGroup(TempleBuffers.GroupReg groupReg){
        templeService.registerGroup(groupReg, new GroupCallback());
    }
    public void setGroupRegisteredListener(OnGroupRegisteredListener groupRegisteredListener){
        this.groupRegisteredListener = groupRegisteredListener;
    }

    public void searchForFriends(TempleBuffers.SearchQuery searchQuery){
        templeService.searchForFriends(searchQuery, new SearchResponseCallback());
    }
    public void setSearchResponseListener(OnSearchResponseListener searchResponseListener){
        this.searchResponseListener = searchResponseListener;
    }

    public void uploadFile(File file){
        FileNameMap fileNameMap = URLConnection.getFileNameMap();
        String mimeType = fileNameMap.getContentTypeFor(file.getName());
        TypedFile typedFile = new TypedFile(mimeType,file);
        // alternatively ["multipart/form-data"]
        templeService.fileUpload(typedFile, new AttachmentCallback());
    }
    public void setFileUploadedListener(OnFileUploadedListener fileUploadedListener){
        this.fileUploadedListener = fileUploadedListener;
    }

    public void downloadFile(String fileName){
        try {
            fileName = String.format("Downloads/%s",fileName);
            FileHandler fileHandler = new FileHandler(fileName);
            byte [] data = templeService.getFile(fileName).body().bytes();
            fileHandler.storeData(data);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }





    private class UserDataCallback implements Callback<UserData>{
        @Override
        public void success(UserData userData, Response response){
            Log.i("USER", userData.toString());
            if(registrationListener != null) {
                registrationListener.onRegistered(userData);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if(registrationListener != null) {
                registrationListener.onRegistrationFailed(error.getMessage());
            }
        }
    }
    private class InvitationCallback implements Callback<String>{
        @Override
        public void success(String s, Response response) {
            if(invitationSentListener != null) {
                invitationSentListener.onInvitationSent(s);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if(invitationSentListener != null) {
                invitationSentListener.onInvitationSendFailed(error.getMessage());
            }
        }
    }

    private class GroupCallback implements Callback<TempleBuffers.Group>{
        @Override
        public void success(TempleBuffers.Group group, Response response) {
            if(groupRegisteredListener != null){
                groupRegisteredListener.onGroupRegistered(group);
            }
        }

        @Override
        public void failure(RetrofitError error) {
                if(groupRegisteredListener != null){
                    groupRegisteredListener.onGroupRegistrationFailed(error.getMessage());
                }
        }
    }
    private class SearchResponseCallback implements Callback<TempleBuffers.SearchResponse>{
        @Override
        public void success(TempleBuffers.SearchResponse searchResponse, Response response){

            if(searchResponseListener != null){
                searchResponseListener.onFriendsFound(searchResponse);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if(searchResponseListener != null){
                searchResponseListener.onSearchFailed(error.getMessage());
            }
        }
    }
    private class AttachmentCallback implements Callback<TempleBuffers.Attachment>{
        @Override
        public void success(TempleBuffers.Attachment attachment, Response response) {
            if(fileUploadedListener != null){
                fileUploadedListener.onFileUploaded(attachment);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            if(fileUploadedListener != null){
                fileUploadedListener.onFileUploadFailed(error.getMessage());
            }
        }
    }
}
