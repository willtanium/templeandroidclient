package com.willtanium.services;

/**
 * Created by william on 7/17/15.
 */
public interface OnInvitationSentListener{
    void onInvitationSent(String response);
    void onInvitationSendFailed(String message);
}
