package com.willtanium.broker;

/**
 * Created by william on 6/15/15.
 */
public interface OnMessageSentListener {
    void onBrokerMessageSent(String messageNote);
    void onBrokerMessageSendFailed(String error);
}
