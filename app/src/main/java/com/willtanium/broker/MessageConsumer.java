package com.willtanium.broker;

import com.google.protobuf.InvalidProtocolBufferException;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.GroupDB;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class MessageConsumer implements MqttCallback {
    private MqttClient androidClient;
    private List<GroupDB> groups;

    private OnPayloadReceivedListener payloadReceivedListener;
    public MessageConsumer(MqttClient androidClient){
        this.androidClient = androidClient;
        androidClient.setCallback(this);
    }

    public void addGroups(List<GroupDB> groups){
        this.groups.addAll(groups);
    }

    public void setPayloadReceivedListener(OnPayloadReceivedListener payloadReceivedListener){
        this.payloadReceivedListener = payloadReceivedListener;
    }

    public void addKeyToQueue(String key){
        try {
            androidClient.subscribe(key, 1);
        } catch (MqttException e) {
            e.printStackTrace();
        }

    }
    public void removeKeyFromQueue(String key){
        try {
            androidClient.unsubscribe(key);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void dispose(){
        if (androidClient.isConnected()) {
            try {
                androidClient.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }


    private void messageFilter(String topic, byte[] payload) {
        try {
            payloadReceivedListener.onMessage(topic, TempleBuffers.ChatMessage.parseFrom(payload));
        } catch (InvalidProtocolBufferException e){
            // It is an invitation
            e.printStackTrace();
                handlerInvitation(payload);
        }

    }
    private void handlerInvitation(byte [] payload){
        try {
            payloadReceivedListener.onInvitation(TempleBuffers.Invitation.parseFrom(payload));
        }catch (InvalidProtocolBufferException e){
            e.printStackTrace();
        }
    }

    public void publishMessage(String topic, byte[] message) {
        try {
            androidClient.publish(topic, new MqttMessage(message));
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {

        payloadReceivedListener.onConnectionLost(throwable.getMessage());
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
        messageFilter(topic, mqttMessage.getPayload());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        payloadReceivedListener.onDeliveryComplete(iMqttDeliveryToken.getResponse().toString());
    }
}
