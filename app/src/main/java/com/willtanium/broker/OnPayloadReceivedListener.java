package com.willtanium.broker;

import com.willtanium.protobuf.TempleBuffers;

/**
 * Created by william on 6/14/15.
 */
public interface OnPayloadReceivedListener{
    void onInvitation(TempleBuffers.Invitation invitation);

    void onMessage(String topic, TempleBuffers.ChatMessage chatMessage);

    void onConnectionLost(String message);

    void onDeliveryComplete(String message);

}
