package com.willtanium.views.writer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.willtanium.adapters.MessagesAdapter;
import com.willtanium.alerts.MessageAlerts;
import com.willtanium.storage.handlers.AccountHandlerImpl;
import com.willtanium.storage.handlers.DatabaseHelper;
import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.handlers.MessageHandlerImpl;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.temple.OnBackPressedListener;
import com.willtanium.temple.R;
import com.willtanium.temple.TempleActivity;

import java.util.LinkedList;

/**
 * Created by william on 6/14/15.
 */
public class MessageWriter extends WriterBaseFragment implements OnBackPressedListener{
    private MessageProperties msgPty;
    private byte [] senderImage;
    private AccountDB user;
    private View view;
    private MessageHandler messageHandler;
    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle){
        view = layoutInflater.inflate(R.layout.message_writer_layout, container, false);
        messageList = (ListView)view.findViewById(R.id.message_list);
        messageEdit = (EditText)view.findViewById(R.id.message_edit);
        sendBtn = (Button)view.findViewById(R.id.send_btn);
        sendBtn.setOnClickListener(this);
        user = AccountHandlerImpl.getAccountHandler().getUser();
        messageDBs = new LinkedList<>();
        getDateFromBundle(getArguments());
        messageList.setAdapter(new MessagesAdapter(messageDBs, senderImage, user.getPhoto()));
        ((TempleActivity)getActivity()).setMessageReceivedListener(this);
        ((TempleActivity) getActivity()).setOnBackPressedListener(this);
        return view;
    }

    public void getDateFromBundle(Bundle bundle){
        try {
            msgPty = (MessageProperties) bundle.getSerializable("MSG_PTY");
            commKey = msgPty.getCommKey();
            senderImage = msgPty.getSenderImage();
            DatabaseHelper dbHelper = ((TempleActivity) getActivity()).getHelper();
            messageHandler = new MessageHandlerImpl(dbHelper);
            messageDBs.addAll(messageHandler.getConversation(commKey));
        }catch (NullPointerException e){
               // Set the default Data
        }

    }

    @Override
    public void onClick(View v){
            messageSender.setMessageSentListener(this);
            sendMessage(((TempleActivity) getActivity()).getAttachments());
    }

    @Override
    public void onMessageReceived(String commID, MessageDB messageDB) {
            if(commID.equals(commKey)){
                updateMessages(messageDB);
            }else{
                ((TempleActivity)getActivity()).messageNotifyByID(messageDB.getMessage(),commID);
            }
    }

    @Override
    public void onBrokerMessageSent(String messageNote) {
        MessageAlerts.messageSent(getActivity(),messageNote);
        ((TempleActivity)getActivity()).eraseAttachments();
    }

    @Override
    public void onBrokerMessageSendFailed(String error) {
        MessageAlerts.messageSendFailed(getActivity(),
                String.format("Failed to send message \n %s", error));
    }

    @Override
    public void backButtonPressed(boolean isPressed) {
        ((TempleActivity)getActivity()).finish();
    }
}
