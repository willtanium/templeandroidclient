package com.willtanium.views.writer;

import com.willtanium.storage.models.MessageDB;

/**
 * Created by william on 6/15/15.
 */
public interface OnMessageReceivedListener {

    void onMessageReceived(String commID, MessageDB messageDB);

}
