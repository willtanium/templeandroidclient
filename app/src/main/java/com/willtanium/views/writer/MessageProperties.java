package com.willtanium.views.writer;

import java.io.Serializable;

/**
 * Created by william on 6/15/15.
 */
public class MessageProperties implements Serializable {
    public String getCommKey() {
        return commKey;
    }

    public byte[] getSenderImage() {
        return senderImage;
    }

    private String commKey;
    private byte [] senderImage;

    public MessageProperties(String commKey, byte[] senderImage) {
        this.commKey = commKey;
        this.senderImage = senderImage;
    }

}
