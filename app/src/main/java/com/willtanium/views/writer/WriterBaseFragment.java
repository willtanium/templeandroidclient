package com.willtanium.views.writer;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.willtanium.adapters.MessagesAdapter;
import com.willtanium.broker.OnMessageSentListener;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.temple.TempleActivity;

import java.util.Date;
import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public abstract class WriterBaseFragment extends Fragment implements View.OnClickListener, OnMessageReceivedListener,OnMessageSentListener {
    protected EditText messageEdit;
    protected Button sendBtn;
    protected ListView messageList;
    protected String commKey;
    protected List<MessageDB> messageDBs;
    protected AccountDB user;


    protected void sendMessage(List<TempleBuffers.Attachment> attachments){
        String messageData = messageEdit.getText().toString();
        String receiverKey = commKey;
        TempleBuffers.ChatMessage.Builder chatMsgBuilder = TempleBuffers.ChatMessage.newBuilder();
        if(!attachments.isEmpty()){
        for(TempleBuffers.Attachment attachment:attachments){
            chatMsgBuilder.addAttachments(attachment);

        }
        }
        chatMsgBuilder.setMessageData(messageData);
        chatMsgBuilder.setSenderId(user.getAccountKey());
        chatMsgBuilder.setTimeStamp(new Date().toString());
        byte data [] = chatMsgBuilder.build().toByteArray();
        ((TempleActivity) getActivity()).sendMessage(receiverKey, data);
    }

    protected void updateMessages(MessageDB messageDB){
        ((MessagesAdapter)messageList.getAdapter()).addMessage(messageDB);
    }

}
