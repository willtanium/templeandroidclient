package com.willtanium.views.forms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.willtanium.alerts.FeatureAlert;
import com.willtanium.alerts.ValidityAlert;
import com.willtanium.temple.R;
import com.willtanium.temple.RegistrationActivity;
import com.willtanium.utilities.Loaders;

import java.io.ByteArrayOutputStream;

/**
 * Created by william on 6/14/15.
 */
public class RegistrationForm extends Fragment implements View.OnClickListener{
    private ImageView previewImage;
    private EditText firstNameEdit;
    private EditText lastNameEdit;
    private EditText emailAddress;
    private EditText phoneNumberEdit;
    private Button registerBtn;
    private View view;
    private byte [] photoData;
    static final int REQUEST_TAKE_PHOTO = 11111;
    private ByteArrayOutputStream stream;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle){
        view = layoutInflater.inflate(R.layout.registration_form_view,container, false);
        previewImage = (ImageView)view.findViewById(R.id.preview_image);
        firstNameEdit = (EditText)view.findViewById(R.id.first_name_edit);
        lastNameEdit = (EditText)view.findViewById(R.id.last_name_edit);
        emailAddress = (EditText)view.findViewById(R.id.email_address_edit);
        phoneNumberEdit = (EditText)view.findViewById(R.id.phone_number_edit);
        registerBtn = (Button)view.findViewById(R.id.register_button);
        previewImage.setOnClickListener(this);
        registerBtn.setOnClickListener(this);
        stream = new ByteArrayOutputStream();

        return view;
    }

    private void takePhoto(){
        Context context = getActivity();
        PackageManager packageManager = context.getPackageManager();
        if(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA) == false){
            FeatureAlert.deviceNotFound(context, "This device does not have a camera.");
            return;
        }
        // Camera exists? Then proceed...
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap =  (Bitmap)data.getExtras().get("data");
            stream.reset();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            photoData = stream.toByteArray();
            previewImage.setImageBitmap(Loaders.byteImageLoader(photoData,
                    previewImage.getWidth(),
                    previewImage.getHeight()));

        }
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.preview_image:{ takePhoto(); break;}
            case R.id.register_button:{ takeUpDetails(); break;}
        }

    }

    private void takeUpDetails(){
                if(validForm()) {

                    ((RegistrationActivity) getActivity()).takeInDetails(firstName,
                            lastName, phone, email, photoData);
                }else{
                   ValidityAlert.formInvalid(getActivity().getApplicationContext(),
                                             " Some Fields are incomplete");
                }
    }
    private boolean validForm(){
        firstName = firstNameEdit.getText().toString();
        lastName = lastNameEdit.getText().toString();
        phone = phoneNumberEdit.getText().toString();
        email = emailAddress.getText().toString();
        if(!firstName.isEmpty() && !lastName.isEmpty() && !phone.isEmpty() && !email.isEmpty()){
            if(photoData.length > 0){
                return true;
            }
        }
        return false;
    }
}
