package com.willtanium.views.forms;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.willtanium.temple.InvitationMgtActivity;
import com.willtanium.temple.OnBackPressedListener;
import com.willtanium.temple.R;
import com.willtanium.views.displays.FriendSelectionView;

/**
 * Created by william on 6/14/15.
 */
public class InvitationCreationFragment extends Fragment implements View.OnClickListener, OnBackPressedListener {
    private EditText prvtMessage;
    private Button sendInvites;
    private View view;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.invitation_creation_layout, container, false);
        sendInvites = (Button) view.findViewById(R.id.send_invitation);
        prvtMessage = (EditText) view.findViewById(R.id.personal_message);
        sendInvites.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        ((InvitationMgtActivity) getActivity()).sendInvitation(prvtMessage.getText().toString());
    }

    @Override
    public void backButtonPressed(boolean isPressed) {
        ((InvitationMgtActivity) getActivity()).shiftToFragment(new FriendSelectionView());
    }
}
