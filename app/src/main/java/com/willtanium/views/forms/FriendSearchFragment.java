package com.willtanium.views.forms;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.willtanium.temple.FriendsMgtActivity;
import com.willtanium.temple.R;
import com.willtanium.views.displays.FriendsList;

/**
 * Created by william on 6/14/15.
 */
public class FriendSearchFragment extends Fragment implements View.OnClickListener {
    private EditText searchTermEdit;
    private Button searchBTN, cancelSearchBtn;
    private View view;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.friend_search_layout, container, false);
        searchTermEdit = (EditText) view.findViewById(R.id.phone_number);
        searchBTN = (Button) view.findViewById(R.id.search_btn);
        cancelSearchBtn = (Button) view.findViewById(R.id.cancel_search);
        cancelSearchBtn.setOnClickListener(this);
        searchBTN.setOnClickListener(this);

        return view;
    }


    public void performSearch(String searchTerm) {
        boolean isValid = searchTermEdit.getText().toString().trim().length() > 0 ? true : false;
        if (isValid) {
            ((FriendsMgtActivity) getActivity()).performSearch(searchTerm);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn: {
                performSearch(searchTermEdit.getText().toString().trim());

                break;
            }
            case R.id.cancel_search: {
                ((FriendsMgtActivity) getActivity()).shiftToFragment(new FriendsList());
                break;
            }
        }
    }
}
