package com.willtanium.views.forms;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.protobuf.ByteString;
import com.turhanoz.android.reactivedirectorychooser.event.OnDirectoryCancelEvent;
import com.turhanoz.android.reactivedirectorychooser.event.OnDirectoryChosenEvent;
import com.turhanoz.android.reactivedirectorychooser.ui.OnDirectoryChooserFragmentInteraction;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.handlers.AccountHandlerImpl;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.temple.GroupMgtActivity;
import com.willtanium.temple.R;
import com.willtanium.utilities.Loaders;
import com.willtanium.views.displays.GroupsList;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by william on 6/14/15.
 */
public class GroupCreationFragment extends Fragment implements OnDirectoryChooserFragmentInteraction,
        View.OnClickListener {
    private TempleBuffers.GroupReg groupReg;
    private EditText groupName;
    private ImageView groupLogo;
    private EditText groupDescription;
    private static int MAX_FILE_SIZE;
    private Button registerGRP;
    private Button cancelGRP;
    private ByteArrayOutputStream byteOPS;
    private FileInputStream fileInputStream;
    private AccountDB user = AccountHandlerImpl.getAccountHandler().getUser();
    private View view;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        byteOPS = new ByteArrayOutputStream();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.group_creation_layout, container, false);
        groupName = (EditText) view.findViewById(R.id.group_name_edit);
        groupLogo = (ImageView) view.findViewById(R.id.group_logo);
        groupDescription = (EditText) view.findViewById(R.id.description_edit);
        registerGRP = (Button) view.findViewById(R.id.register);
        cancelGRP = (Button) view.findViewById(R.id.cancel);
        registerGRP.setOnClickListener(this);
        cancelGRP.setOnClickListener(this);
        return view;
    }

    private TempleBuffers.GroupReg createGroup(String groupName, String groupDesc, byte[] image) {
        TempleBuffers.GroupReg.Builder grpBuilder = TempleBuffers.GroupReg.newBuilder();
        grpBuilder.setCreatorKey(user.getAccountKey());
        grpBuilder.setGroupName(groupName);
        grpBuilder.setGroupDescription(groupDesc);
        grpBuilder.setGroupLogo(ByteString.copyFrom(image, 0, image.length));
        return grpBuilder.build();
    }

    @Override
    public void onEvent(OnDirectoryChosenEvent onDirectoryChosenEvent) {
        File file = onDirectoryChosenEvent.getFile();
        byte[] buffer = new byte[1024];
        int read = 0;
        if (!file.isDirectory() && file.canRead()) {
            try {

                fileInputStream = new FileInputStream(file);
                if (file.length() > MAX_FILE_SIZE) {
                    // Fire Notification
                } else {
                    while ((read = fileInputStream.read(buffer)) != -1) {
                        byteOPS.write(buffer, 0, read);
                    }
                    fileInputStream.close();
                    byteOPS.close();
                    groupLogo.setImageBitmap(Loaders.byteImageLoader(byteOPS.toByteArray(),
                            groupLogo.getWidth(), groupLogo.getHeight()));
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onEvent(OnDirectoryCancelEvent onDirectoryCancelEvent) {

    }

    private boolean validateForm() {
        boolean groupNameCheck = groupName.getText().toString().trim().length() > 0;
        boolean descriptionCheck = groupDescription.getText().toString().trim().length() > 0;
        boolean groupLogoCheck = byteOPS.toByteArray().length > 0 ? true : false;
        if (groupNameCheck && descriptionCheck && groupLogoCheck) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register: {
                if (validateForm()) {
                    groupReg = createGroup(groupName.getText().toString(),
                            groupDescription.getText().toString(),
                            byteOPS.toByteArray());
                    ((GroupMgtActivity) getActivity()).sendGroupDetails(groupReg);
                }
            }
            case R.id.cancel: {
                ((GroupMgtActivity) getActivity()).shiftToFragment(new GroupsList());
            }
        }

    }
}
