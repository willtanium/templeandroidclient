package com.willtanium.views.displays;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.alertdialogpro.AlertDialogPro;
import com.willtanium.adapters.GroupsAdapter;
import com.willtanium.storage.handlers.GroupHandler;
import com.willtanium.storage.handlers.GroupHandlerImpl;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.temple.GroupMgtActivity;
import com.willtanium.temple.R;
import com.willtanium.utilities.Converters;

/**
 * Created by william on 6/14/15.
 */
public class GroupsList extends Fragment implements TextWatcher, AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener, DialogInterface.OnClickListener,
        DialogInterface.OnDismissListener {
    private GroupsAdapter groupsAdapter;
    private ListView listView;
    private EditText entitySearch;
    private GroupHandler groupHandler;
    private GroupDB groupDB;
    private View view;
    private AlertDialogPro.Builder dialog;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        groupHandler = new GroupHandlerImpl(((GroupMgtActivity) getActivity()).getHelper());
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.entity_list_layout, container, false);
        listView = (ListView) view.findViewById(R.id.entity_list);
        entitySearch = (EditText) view.findViewById(R.id.entity_search);
        entitySearch.addTextChangedListener(this);
        groupsAdapter = new GroupsAdapter(groupHandler.getAllGroups());
        listView.setAdapter(groupsAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        groupsAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        groupDB = (GroupDB) parent.getItemAtPosition(position);
        dialog = new AlertDialogPro.Builder(getActivity());
        dialog.setTitle(groupDB.getGroupName());
        Bitmap image = Converters.getBitmapFromBytes(groupDB.getGroupPhoto());
        Drawable drawable = new BitmapDrawable(getResources(), image);
        dialog.setMessage(groupDB.getGroupDescription());
        dialog.setIcon(drawable);
        dialog.setOnDismissListener(this);
        dialog.setPositiveButton("CHAT", this);
        dialog.setNegativeButton("CANCEL", this);
        dialog.show();

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        groupDB = (GroupDB) parent.getItemAtPosition(position);
        dialog = new AlertDialogPro.Builder(getActivity());
        dialog.setPositiveButton(R.string.YES, this);
        dialog.setNegativeButton(R.string.NO, this);
        dialog.setOnDismissListener(this);
        return false;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        groupsAdapter = new GroupsAdapter(groupHandler.getAllGroups());
        listView.setAdapter(groupsAdapter);
        groupsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == R.string.YES) {
            groupHandler.removeGroup(groupDB.getGroupKey());
        }
        if (which == R.string.CHAT) {
            ((GroupMgtActivity) getActivity()).jumpToChat(groupDB.getGroupKey());
        }
        dialog.dismiss();
    }
}
