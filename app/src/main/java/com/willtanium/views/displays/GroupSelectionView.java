package com.willtanium.views.displays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.willtanium.adapters.GroupsAdapter;
import com.willtanium.storage.handlers.GroupHandler;
import com.willtanium.storage.handlers.GroupHandlerImpl;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.temple.InvitationMgtActivity;
import com.willtanium.temple.OnBackPressedListener;
import com.willtanium.temple.R;

/**
 * Created by william on 7/22/15.
 */
public class GroupSelectionView extends Fragment implements TextWatcher, OnBackPressedListener, AdapterView.OnItemClickListener {
    private EditText filter;
    private ListView groupList;
    private GroupsAdapter groupsAdapter;
    private GroupHandler groupHandler;
    private View view;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        groupHandler = new GroupHandlerImpl(((InvitationMgtActivity) getActivity()).getHelper());
        groupsAdapter = new GroupsAdapter(groupHandler.getAllGroups());
        ((InvitationMgtActivity) getActivity()).setBackPressedListener(this);
        groupList.setOnItemClickListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.entity_list_layout, container, false);
        groupList = (ListView) view.findViewById(R.id.entity_list);
        filter = (EditText) view.findViewById(R.id.filter);
        groupList.setAdapter(groupsAdapter);
        filter.addTextChangedListener(this);

        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        groupsAdapter.getFilter().filter(s);
    }

    @Override
    public void backButtonPressed(boolean isPressed) {
        ((InvitationMgtActivity) getActivity()).finish();

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GroupDB groupDB = (GroupDB) parent.getItemAtPosition(position);
        ((InvitationMgtActivity) getActivity()).setGroup(groupDB);
        ((InvitationMgtActivity) getActivity()).shiftToFragment(new FriendSelectionView());
    }
}
