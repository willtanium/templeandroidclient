package com.willtanium.views.displays;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.alertdialogpro.AlertDialogPro;
import com.willtanium.adapters.InvitationsAdapter;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.handlers.InvitationHandler;
import com.willtanium.storage.handlers.InvitationHandlerImpl;
import com.willtanium.temple.InvitationMgtActivity;
import com.willtanium.temple.R;
import com.willtanium.utilities.Converters;

/**
 * Created by william on 6/14/15.
 */
public class InvitationsList extends Fragment implements TextWatcher, AdapterView.OnItemClickListener,
        DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
    private InvitationHandler invitationHandler;
    private InvitationsAdapter invitationsAdapter;
    private ListView listView;
    private View view;
    private EditText filter;
    private AlertDialogPro.Builder invitationDialog;
    private TempleBuffers.Invitation invitation;



    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        invitationHandler = new InvitationHandlerImpl();
    }


    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.invitation_list_layout, container, false);
        listView = (ListView) view.findViewById(R.id.invitation_list);
        filter = (EditText) view.findViewById(R.id.filter);
        invitationsAdapter = new InvitationsAdapter(invitationHandler.getInvitations());
        listView.setAdapter(invitationsAdapter);
        listView.setOnItemClickListener(this);
        filter.addTextChangedListener(this);
        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        invitationsAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        invitation = (TempleBuffers.Invitation) parent.getItemAtPosition(position);
        invitationDialog = new AlertDialogPro.Builder(getActivity());
        invitationDialog.setTitle(invitation.getGroup().getGroupName());
        invitationDialog.setMessage(invitation.getPersonalMessage());
        Bitmap image = Converters.getBitmapFromBytes(invitation.getGroup()
                .getGroupLogo().toByteArray());
        Drawable drawable = new BitmapDrawable(getResources(), image);
        invitationDialog.setIcon(drawable);
        invitationDialog.setPositiveButton(R.string.ACCEPT, this);
        invitationDialog.setNegativeButton(R.string.DENY, this);
        invitationDialog.setOnDismissListener(this);
        invitationDialog.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == R.string.ACCEPT) {
            invitationHandler
                    .acceptInvitation(((InvitationMgtActivity) getActivity()).getHelper(),
                            invitation);
        }
        if (which == R.string.DENY) {
            invitationHandler.removeInvitation(invitation);
        }
        dialog.dismiss();


    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        invitationsAdapter = new InvitationsAdapter(invitationHandler.getInvitations());
        listView.setAdapter(invitationsAdapter);
    }
}
