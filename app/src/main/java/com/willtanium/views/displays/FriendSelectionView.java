package com.willtanium.views.displays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.willtanium.adapters.FriendSelectAdapter;
import com.willtanium.storage.handlers.FriendHandler;
import com.willtanium.storage.handlers.FriendHandlerImpl;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.temple.InvitationMgtActivity;
import com.willtanium.temple.OnBackPressedListener;
import com.willtanium.temple.R;
import com.willtanium.views.forms.InvitationCreationFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 7/22/15.
 */
public class FriendSelectionView extends Fragment implements View.OnClickListener, OnBackPressedListener {
    private EditText filter;
    private ListView friendsList;
    private FriendSelectAdapter friendSelectAdapter;
    private FriendHandler friendHandler;
    private List<FriendDB> selected;
    private Button nextBtn;
    private View view;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        friendHandler = new FriendHandlerImpl(((InvitationMgtActivity) getActivity()).getHelper());
        friendSelectAdapter = new FriendSelectAdapter(friendHandler.getAllFriends());
        selected = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.friends_select_layout, container, false);
        filter = (EditText) view.findViewById(R.id.filter);
        nextBtn = (Button) view.findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(this);
        friendsList = (ListView) view.findViewById(R.id.friend_select_list);
        return view;
    }


    @Override
    public void onClick(View v) {
        if (R.id.next_btn == v.getId()) {

            ((InvitationMgtActivity) getActivity())
                    .setSelectedFriends(friendSelectAdapter.getChoiceArray());
            ((InvitationMgtActivity) getActivity()).shiftToFragment(new InvitationCreationFragment());
        }
    }

    @Override
    public void backButtonPressed(boolean isPressed) {
        ((InvitationMgtActivity) getActivity()).shiftToFragment(new GroupSelectionView());
    }
}
