package com.willtanium.views.displays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.willtanium.adapters.GroupsMessagesAdapter;
import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.temple.R;
import com.willtanium.temple.TempleActivity;

import java.util.List;

/**
 * Created by william on 7/19/15.
 */
public class GroupMessageView extends Fragment implements AdapterView.OnItemClickListener, TextWatcher {
    private ListView listView;
    private View view;
    private EditText filter;
    private GroupsMessagesAdapter groupsMessagesAdapter;
    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle){
            view  = layoutInflater.inflate(R.layout.message_listview_layout, container, false);
            listView = (ListView)view.findViewById(R.id.message_list);
        filter = (EditText) view.findViewById(R.id.filter);
        filter.addTextChangedListener(this);
            listView.setAdapter(groupsMessagesAdapter);
            listView.setOnItemClickListener(this);
        return view;
    }

    public void addGroups(List<GroupDB> groupDBs, MessageHandler messageHandler){
        groupsMessagesAdapter = new GroupsMessagesAdapter(groupDBs, messageHandler);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ((TempleActivity)getActivity()).openMessageWriter(((GroupDB)parent.getSelectedItem()).getGroupKey());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        groupsMessagesAdapter.getFilter().filter(s);
    }
}
