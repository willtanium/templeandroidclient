package com.willtanium.views.displays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.willtanium.adapters.ViewPageAdapter;
import com.willtanium.temple.OnBackPressedListener;
import com.willtanium.temple.R;
import com.willtanium.temple.TempleActivity;

/**
 * Created by william on 6/14/15.
 */
public class MessagesList extends Fragment implements OnBackPressedListener{
    private ViewPager viewPager;
    private View view;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup container, Bundle bundle){
        view = layoutInflater.inflate(R.layout.message_select_layout, container, false);
        viewPager = (ViewPager)view.findViewById(R.id.message_select);
        viewPager.setAdapter(new ViewPageAdapter(getFragmentManager(),
                ((TempleActivity)getActivity()).getHelper()));
        ((TempleActivity) getActivity()).setOnBackPressedListener(this);
        return view;
    }

    @Override
    public void backButtonPressed(boolean isPressed) {
        ((TempleActivity)getActivity()).finish();
    }
}
