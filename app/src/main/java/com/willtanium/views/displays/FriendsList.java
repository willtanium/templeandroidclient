package com.willtanium.views.displays;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.alertdialogpro.AlertDialogPro;
import com.willtanium.adapters.FriendsAdapter;
import com.willtanium.storage.handlers.FriendHandler;
import com.willtanium.storage.handlers.FriendHandlerImpl;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.temple.FriendsMgtActivity;
import com.willtanium.temple.R;
import com.willtanium.utilities.Converters;

/**
 * Created by william on 6/14/15.
 */
public class FriendsList extends Fragment implements TextWatcher, AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener {
    private FriendHandler friendHandler;
    private ListView listView;
    private EditText entitySearch;
    private FriendsAdapter friendsAdapter;
    private View view;
    private AlertDialogPro.Builder dialog;
    private FriendDB friendDB;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        friendHandler = new FriendHandlerImpl(((FriendsMgtActivity) getActivity()).getHelper());

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        view = layoutInflater.inflate(R.layout.entity_list_layout, viewGroup, false);
        listView = (ListView) view.findViewById(R.id.entity_list);
        entitySearch = (EditText) view.findViewById(R.id.entity_search);
        entitySearch.addTextChangedListener(this);
        friendsAdapter = new FriendsAdapter(friendHandler.getAllFriends());
        listView.setAdapter(friendsAdapter);
        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);
        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        friendsAdapter.getFilter().filter(s);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        friendDB = (FriendDB) parent.getItemAtPosition(position);
        dialog = new AlertDialogPro.Builder(getActivity());
        Bitmap bitmap = Converters.getBitmapFromBytes(friendDB.getPhoto());
        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
        dialog.setIcon(drawable);
        dialog.setPositiveButton(R.string.CHAT, this);
        dialog.setNegativeButton(R.string.CANCEL, this);
        dialog.setOnDismissListener(this);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

        return false;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == R.string.CHAT) {
            ((FriendsMgtActivity) getActivity()).jumpToChat(friendDB.getFriendKey());
        }


    }

    @Override
    public void onDismiss(DialogInterface dialog) {

    }
}
