package com.willtanium.temple;

/**
 * Created by william on 7/19/15.
 */
public interface OnBackPressedListener {
    void backButtonPressed(boolean isPressed);
}
