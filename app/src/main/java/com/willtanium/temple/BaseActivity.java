package com.willtanium.temple;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;
import com.heinrichreimersoftware.materialdrawer.DrawerView;
import com.heinrichreimersoftware.materialdrawer.structure.DrawerItem;
import com.willtanium.broker.MessageConsumer;
import com.willtanium.broker.OnPayloadReceivedListener;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.services.TempleServiceHandler;
import com.willtanium.storage.handlers.AccountHandlerImpl;
import com.willtanium.storage.handlers.DatabaseHelper;
import com.willtanium.storage.handlers.FriendHandler;
import com.willtanium.storage.handlers.FriendHandlerImpl;
import com.willtanium.storage.handlers.GroupHandler;
import com.willtanium.storage.handlers.GroupHandlerImpl;
import com.willtanium.storage.handlers.InvitationHandler;
import com.willtanium.storage.handlers.InvitationHandlerImpl;
import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.handlers.MessageHandlerImpl;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.utilities.Converters;
import com.willtanium.views.writer.OnMessageReceivedListener;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.util.List;

/**
 * Created by william on 7/17/15.
 */
public abstract class BaseActivity extends OrmliteDrawerActivity<DatabaseHelper>
        implements OnPayloadReceivedListener,DrawerItem.OnItemClickListener{
    // have to create drawer listeners for the jump in between activities without warning
    // Each Activity should be unique in its abilities but share the ability to show current
    // Stuff that may be on going say messages that have just come in.
    protected OnMessageReceivedListener messageReceivedListener;
    protected MessageHandler messageHandler;
    protected TempleServiceHandler templeServiceHandler;
    protected InvitationHandler invitationHandler;
    protected AccountDB user;
    protected MessageConsumer messageConsumer;
    protected List<GroupDB> groupDBs;
    protected GroupHandler groupHandler;
    protected Intent intent;
    protected DrawerView drawer;
    protected Toolbar toolbar;
    protected DrawerLayout drawerLayout;
    protected ActionBarDrawerToggle drawerToggle;
    protected Intent prevIntent;
    private MqttConnectOptions options;
    private MqttClient androidClient;

    @Override
    protected void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        options = new MqttConnectOptions();

        user = AccountHandlerImpl.getAccountHandler().getUser();
        messageHandler = new MessageHandlerImpl(getHelper());
        invitationHandler = new InvitationHandlerImpl();
        templeServiceHandler = new TempleServiceHandler(this);
        groupHandler = new GroupHandlerImpl(getHelper());
        try {
            androidClient = new MqttClient(getResources()
                    .getString(R.string.mqtt_point), MqttClient.generateClientId(),null);
            androidClient.connect(options);
            messageConsumer = new MessageConsumer(androidClient);
            messageConsumer.addKeyToQueue(user.getAccountKey());
            messageConsumer.setPayloadReceivedListener(this);
        } catch (MqttException e) {
            e.printStackTrace();
        }
        initializeListening();

    }
    public MqttClient getAndroidClient(){
        return androidClient;
    }

    public MessageConsumer getMessageHandler() {
        return messageConsumer;
    }
    private void initializeListening(){
        try {
            groupDBs = groupHandler.getAllGroups();
            messageConsumer.addGroups(groupDBs);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toolbar = (Toolbar)findViewById(R.id.main_toolbar);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        drawer = (DrawerView)findViewById(R.id.drawer);
        drawerLoader(drawer);

        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.open_drawer,
                R.string.close_drawer);
        if(drawerLayout != null) {
            drawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.abc_primary_text_material_dark));

            drawerToggle.setHomeAsUpIndicator(R.mipmap.ic_drawer);
            drawerLayout.setDrawerListener(drawerToggle);
            drawerLayout.closeDrawer(drawer);
            drawer.setOnItemClickListener(this);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    public void shiftToFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }

    public void setMessageReceivedListener(OnMessageReceivedListener messageReceivedListener){
        this.messageReceivedListener = messageReceivedListener;
    }

    @Override
    public void onInvitation(TempleBuffers.Invitation invitation) {
        invitationHandler.addInvitation(invitation);
        NiftyNotificationView.build(this, String.format("Group :%s\n" +
                                "PM %s", invitation.getPersonalMessage(),
                        invitation.getPersonalMessage()),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.tickets)         //You must call this method if you use ThumbSlider effect
                .show();

    }

    @Override
    public void onMessage(String topic, TempleBuffers.ChatMessage chatMessage) {
        MessageDB messageDB = messageHandler.saveMessage(chatMessage);
        if(messageReceivedListener != null){
            messageReceivedListener.onMessageReceived(chatMessage.getSenderId(),messageDB);
        }else {
            messageNotifyByID(chatMessage.getMessageData(),chatMessage.getSenderId());
        }
    }

    @Override
    public void onConnectionLost(String message) {
        NiftyNotificationView.build(this, String.format("Error %s ", message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                .show();
    }

    @Override
    public void onDeliveryComplete(String message) {
        NiftyNotificationView.build(this, String.format("%s", message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.upload)         //You must call this method if you use ThumbSlider effect
                .show();
    }
    public void messageNotifyByID(String message, String senderId) {
        Bitmap image = Converters.getBitmapFromBytes(getSendersImage(senderId));
        if (image != null) {
            Drawable icon = new BitmapDrawable(getResources(), image);
            NiftyNotificationView.build(this, message,
                    Effects.thumbSlider, R.id.mLyout)
                    .setIcon(icon)         //You must call this method if you use ThumbSlider effect
                    .show();
        }
    }

    public void removeKey(String key) {
        messageConsumer.removeKeyFromQueue(key);
    }

    public void addKey(String key) {
        messageConsumer.addKeyToQueue(key);
    }
    public byte [] getSendersImage(String senderID){
        byte [] image = null;
        GroupHandler groupHandler = new GroupHandlerImpl(getHelper());
        FriendHandler friendHandler = new FriendHandlerImpl(getHelper());
        if((image = groupHandler.getGroup(senderID).getGroupPhoto()) == null){

            image = friendHandler.getFriend(senderID).getPhoto();
        }
        return image;
    }

    public void sendMessage(String topic, byte[] message) {
        messageConsumer.publishMessage(topic, message);
    }
    public void jumpToChat(String senderID) {
        intent = new Intent(this, TempleActivity.class);
        intent.putExtra("SenderID", senderID);
        finish();
        startActivity(intent);
    }

    protected void shiftToSystemActivity(){
        intent = new Intent(this,SystemMgtActivity.class);
        finish();
        startActivity(intent);
    }
    protected void shiftToGroupActivity(){
        intent = new Intent(this,GroupMgtActivity.class);
        finish();
        startActivity(intent);
    }
    protected void shiftToInvitationActivity(){
        intent = new Intent(this,InvitationMgtActivity.class);
        finish();
        startActivity(intent);
    }
    protected void shiftToTempleActivity(){
        intent = new Intent(this,TempleActivity.class);
        finish();
        startActivity(intent);
    }
    protected void shiftToFriendsActivity(){
        intent = new Intent(this,FriendsMgtActivity.class);
        finish();
        startActivity(intent);
    }
    protected void shiftToUtilitiesActivity(){
        intent = new Intent(this,UtilitiesActivity.class);
        finish();
        startActivity(intent);
    }

    private void drawerLoader(DrawerView drawerView){
        try {
            drawerView.addItem(new DrawerItem().setTextPrimary("Friends").
                    setTextSecondary("Get In touch with Friends", DrawerItem.TWO_LINE)
                    .setId(R.integer.friends));
            drawerView.addItem(new DrawerItem().setTextPrimary("Groups").
                    setTextSecondary("Stay close to groups of Interest", DrawerItem.TWO_LINE)
                    .setId(R.integer.groups));
            drawerView.addItem(new DrawerItem().setTextPrimary("Invitations").setId(R.integer.invitations));
            drawerView.addItem(new DrawerItem().setTextPrimary("Utilities").setId(R.integer.utilities));
            drawerView.addFixedItem(new DrawerItem().setTextPrimary("Settings").setId(R.integer.settings));
            drawerView.addFixedItem(new DrawerItem().setTextPrimary("About").setId(R.integer.about));
            drawerView.addFixedItem(new DrawerItem().setTextPrimary("ShutDown").setId(R.integer.exit));
            drawerView.addFixedItem(new DrawerItem().setTextPrimary("Chat Center").setId(R.integer.chat));
        }catch (NullPointerException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(DrawerItem drawerItem, long l, int i) {
        int navID = (int)drawerItem.getId();

        switch (navID){
            case R.integer.friends:{
                shiftToFriendsActivity();
                break;
            }
            case R.integer.groups:{
                shiftToGroupActivity();
                break;
            }
            case R.integer.utilities:{
                shiftToUtilitiesActivity();
                break;
            }
            case R.integer.invitations:{
                shiftToInvitationActivity();
                break;
            }
            case R.integer.settings:{
                shiftToSystemActivity();
                break;
            }
            case R.integer.chat:{
                shiftToTempleActivity();
                break;
            }
            case R.integer.about:{
                // Show dialog with the product information that is cancellable
                break;
            }
            case R.integer.exit:{
                finish();
            }


        }



    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void stopAllServices(){
        try {
            if (isMyServiceRunning(ShadowService.class)) {
                stopService(prevIntent);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();
        stopAllServices();
    }
    @Override
    protected void onStop() {
        super.onStop();
        prevIntent = new Intent(this,ShadowService.class);
        startService(prevIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        messageConsumer.dispose();
    }


}
