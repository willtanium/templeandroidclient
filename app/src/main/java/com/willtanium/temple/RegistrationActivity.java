package com.willtanium.temple;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;
import com.google.protobuf.ByteString;
import com.willtanium.alerts.NetworkAlerts;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.protobuf.TempleBuffers.UserData;
import com.willtanium.protobuf.TempleBuffers.UserReg;
import com.willtanium.services.OnRegisteredListener;
import com.willtanium.services.OnSearchResponseListener;
import com.willtanium.services.TempleServiceHandler;
import com.willtanium.storage.handlers.AccountHandlerImpl;
import com.willtanium.storage.handlers.DatabaseHelper;
import com.willtanium.storage.handlers.FriendHandler;
import com.willtanium.storage.handlers.FriendHandlerImpl;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.utilities.Loaders;
import com.willtanium.views.forms.RegistrationForm;

import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class RegistrationActivity extends OrmliteDrawerActivity<DatabaseHelper>
        implements OnRegisteredListener, OnSearchResponseListener {
    private TempleServiceHandler templeServiceHandler;
    private FriendHandler friendHandler;
    private UserReg.Builder userReg;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.registration_layout);
        templeServiceHandler = new TempleServiceHandler(this);
        templeServiceHandler.setRegistrationListener(this);
        friendHandler = new FriendHandlerImpl(getHelper());
        AccountDB accountDB = AccountHandlerImpl.getAccountHandler().getUser();
        if(accountDB != null){
            goToTempleActivity();
            Log.i("Account",String.format("Name %s Key %s",accountDB.getFirstName(),accountDB.getAccountKey()));
        }
        loadFragment(new RegistrationForm());

    }

    public void loadFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.commit();
    }

    public void takeInDetails(String firstName, String lastName, String phone, String email, byte[] photoData) {
        userReg = UserReg.newBuilder();
        userReg.setFirstName(firstName);
        userReg.setLastName(lastName);
        userReg.setPhoneNumber(phone);
        userReg.setEmailAddress(email);
        userReg.setPhotoData(ByteString.copyFrom(photoData));
        templeServiceHandler.registerUser(userReg.build());


    }


    private void searchFriendsInPhoneBook() {
        List<String> contacts = Loaders.getContacts(this);
        TempleBuffers.SearchQuery.Builder schQry = TempleBuffers.SearchQuery.newBuilder();
        for (String contact : contacts) {
            schQry.addPhoneNumbers(contact);
        }
        templeServiceHandler.searchForFriends(schQry.build());
        templeServiceHandler.setSearchResponseListener(this);
    }
    private void goToTempleActivity(){
        Intent intent = new Intent(this,TempleActivity.class);
        finish();
        startActivity(intent);
    }
    @Override
    public void onRegistered(UserData userData) {
        AccountDB accountDB = new AccountDB(userReg.getFirstName(),
                userReg.getLastName(),
                userReg.getEmailAddress(),
                userReg.getPhoneNumber(),userReg.getPhotoData().toByteArray(),
                userData.getFriendKey());
        AccountHandlerImpl.getAccountHandler().saveUser(accountDB);
        searchFriendsInPhoneBook();
    }

    @Override
    public void onRegistrationFailed(String message) {
        NetworkAlerts.genericError(getApplicationContext(), message);
    }

    @Override
    public void onFriendsFound(TempleBuffers.SearchResponse searchResponse) {
         try {
            if (searchResponse.getFriendsList().size() > 0) {
                for (TempleBuffers.Friend friend : searchResponse.getFriendsList()) {
                    friendHandler.saveFriend(friend);
                }
            } else {
                NiftyNotificationView.build(this, "Friend search turned empty",
                        Effects.thumbSlider, R.id.mLyout)
                        .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                        .show();
            }
        }catch (NullPointerException e){
             e.printStackTrace();

                 NiftyNotificationView.build(this, "Friend search turned empty",
                         Effects.thumbSlider, R.id.mLyout)
                         .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                         .show();

         }
        goToTempleActivity();

    }

    @Override
    public void onSearchFailed(String message) {
        NiftyNotificationView.build(this, String.format("Friend search failed --%s", message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                .show();
        goToTempleActivity();
    }
}
