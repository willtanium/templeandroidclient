package com.willtanium.temple;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;
import com.turhanoz.android.reactivedirectorychooser.event.OnDirectoryCancelEvent;
import com.turhanoz.android.reactivedirectorychooser.event.OnDirectoryChosenEvent;
import com.turhanoz.android.reactivedirectorychooser.ui.DirectoryChooserFragment;
import com.turhanoz.android.reactivedirectorychooser.ui.OnDirectoryChooserFragmentInteraction;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.services.OnFileUploadedListener;
import com.willtanium.views.displays.MessagesList;
import com.willtanium.views.writer.MessageProperties;
import com.willtanium.views.writer.MessageWriter;

import java.util.LinkedList;
import java.util.List;


public class TempleActivity extends BaseActivity implements OnDirectoryChooserFragmentInteraction,
        OnFileUploadedListener {
    private List<TempleBuffers.Attachment> attachments;
    private OnBackPressedListener backPressedListener;
    private DirectoryChooserFragment directoryChooserFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temple);
                attachments = new LinkedList<>();
        try{
            prevIntent = getIntent();
            String senderID = prevIntent.getStringExtra("SenderID");
            openMessageWriter(senderID);
        }catch (NullPointerException e){
            shiftToFragment(new MessagesList());
            Log.i("MESSAGE LIST","Message list activated");
        }

    }

    public void setOnBackPressedListener(OnBackPressedListener backPressedListener) {
        this.backPressedListener = backPressedListener;
    }
    public void openMessageWriter(String senderID){
        Bundle bundle = new Bundle();

        MessageProperties messageProperties = new MessageProperties(senderID,
                getSendersImage(senderID));
        bundle.putSerializable("MSG_PTY", messageProperties);
        MessageWriter messageWriter = new MessageWriter();
        messageWriter.setArguments(bundle);
        shiftToFragment(messageWriter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_temple, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.attach) {

            directoryChooserFragment = DirectoryChooserFragment
                    .newInstance(Environment.getExternalStorageDirectory());
            directoryChooserFragment.show(getSupportFragmentManager(), "RDC");

        }

        return super.onOptionsItemSelected(item);
    }


    public List<TempleBuffers.Attachment> getAttachments(){
        return attachments;
    }


    @Override
    public void onEvent(OnDirectoryChosenEvent onDirectoryChosenEvent) {
        templeServiceHandler.setFileUploadedListener(this);
        templeServiceHandler.uploadFile(onDirectoryChosenEvent.getFile());
    }

    @Override
    public void onEvent(OnDirectoryCancelEvent onDirectoryCancelEvent){

    }

    @Override
    public void onFileUploaded(TempleBuffers.Attachment attachment) {
        NiftyNotificationView.build(TempleActivity.this, String.format("FILE %s UPLOADED",
                        attachment.getAttachmentKey()),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.upload)         //You must call this method if you use ThumbSlider effect
                .show();
        attachments.add(attachment);

    }

    public void eraseAttachments(){
        attachments.clear();
    }

    @Override
    public void onFileUploadFailed(String message) {
        NiftyNotificationView.build(TempleActivity.this,String.format("Error %s on upload",message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                .show();

    }
    @Override
    public void onBackPressed(){
        backPressedListener.backButtonPressed(true);
    }


}
