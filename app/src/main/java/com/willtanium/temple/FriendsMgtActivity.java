package com.willtanium.temple;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.services.OnSearchResponseListener;
import com.willtanium.services.TempleServiceHandler;
import com.willtanium.storage.handlers.FriendHandler;
import com.willtanium.storage.handlers.FriendHandlerImpl;
import com.willtanium.views.displays.FriendsList;
import com.willtanium.views.forms.FriendSearchFragment;

/**
 * Created by william on 6/14/15.
 */
public class FriendsMgtActivity extends BaseActivity implements OnSearchResponseListener {
    private TempleServiceHandler templeServiceHandler;
    private FriendHandler friendHandler;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.friends_main_activity);
        templeServiceHandler = new TempleServiceHandler(this);
        friendHandler = new FriendHandlerImpl(getHelper());
        shiftToFragment(new FriendsList());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.friends_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.friend_search) {
            shiftToFragment(new FriendSearchFragment());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void performSearch(String searchTerm) {
        TempleBuffers.SearchQuery.Builder searchQuery = TempleBuffers.SearchQuery.newBuilder();
        searchQuery.addPhoneNumbers(searchTerm);
        templeServiceHandler.searchForFriends(searchQuery.build());
        templeServiceHandler.setSearchResponseListener(this);

    }

    @Override
    public void onFriendsFound(TempleBuffers.SearchResponse searchResponse) {
        int count = 0;
        for (TempleBuffers.Friend friend : searchResponse.getFriendsList()) {
            friendHandler.saveFriend(friend);
            count++;
        }
        NiftyNotificationView.build(this, String.format(" %d Friends have been added ", count),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.upload)         //You must call this method if you use ThumbSlider effect
                .show();
        shiftToFragment(new FriendsList());
    }

    @Override
    public void onSearchFailed(String message) {
        NiftyNotificationView.build(this, String.format("Friend search failed --%s", message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                .show();

    }
}
