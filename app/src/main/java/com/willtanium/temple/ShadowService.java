package com.willtanium.temple;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteBaseService;
import com.willtanium.broker.MessageConsumer;
import com.willtanium.broker.OnPayloadReceivedListener;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.storage.handlers.AccountHandlerImpl;
import com.willtanium.storage.handlers.DatabaseHelper;
import com.willtanium.storage.handlers.FriendHandler;
import com.willtanium.storage.handlers.FriendHandlerImpl;
import com.willtanium.storage.handlers.GroupHandler;
import com.willtanium.storage.handlers.GroupHandlerImpl;
import com.willtanium.storage.handlers.InvitationHandler;
import com.willtanium.storage.handlers.InvitationHandlerImpl;
import com.willtanium.storage.handlers.MessageHandler;
import com.willtanium.storage.handlers.MessageHandlerImpl;
import com.willtanium.storage.models.AccountDB;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.storage.models.MessageDB;
import com.willtanium.utilities.Converters;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

/**
 * Created by william on 7/18/15.
 */
public class ShadowService extends OrmLiteBaseService<DatabaseHelper> implements OnPayloadReceivedListener{
    protected InvitationHandler invitationHandler;
    protected GroupHandler groupHandler;
    protected FriendHandler friendHandler;
    protected MessageConsumer messageConsumer;
    protected MessageHandler messageHandler;
    protected Notification notification;
    protected NotificationManager mNotificationManager;
    protected static final int notificationID = 1245;
    protected DatabaseHelper dbHelper;
    protected MqttClient androidClient;
    private MqttConnectOptions options;
    private AccountDB user;
    @Override
    public void onCreate() {

        super.onCreate();

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            dbHelper = getHelper();
            user = AccountHandlerImpl.getAccountHandler().getUser();
            invitationHandler = new InvitationHandlerImpl();
            groupHandler = new GroupHandlerImpl(dbHelper);
            friendHandler = new FriendHandlerImpl(dbHelper);
            androidClient = new MqttClient(getResources()
                    .getString(R.string.mqtt_point), MqttClient.generateClientId(),null);
            options = new MqttConnectOptions();
            androidClient.connect(options);
            messageConsumer = new MessageConsumer(androidClient);
            messageConsumer.addKeyToQueue(user.getAccountKey());
            messageConsumer.setPayloadReceivedListener(this);
            messageHandler = new MessageHandlerImpl(dbHelper);
        }catch (IllegalStateException e){
            Log.i("TEMPLE", String.format("%s", e.getMessage()));
            super.onCreate();
        } catch (MqttSecurityException e) {
            e.printStackTrace();
        } catch (MqttException e) {
            e.printStackTrace();
        }

        return START_NOT_STICKY;
    }

    // This is the next step in the construction base
    // the back ground communication point of the application
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public byte [] getSendersImage(String senderID){
        byte [] image = null;
        GroupHandler groupHandler = new GroupHandlerImpl(getHelper());
        FriendHandler friendHandler = new FriendHandlerImpl(getHelper());
        if((image = groupHandler.getGroup(senderID).getGroupPhoto()) == null){

            image = friendHandler.getFriend(senderID).getPhoto();
        }
        return image;
    }
    public String getSendersIdentity(String senderID){
        String name = "";
        GroupHandler groupHandler = new GroupHandlerImpl(getHelper());
        FriendHandler friendHandler = new FriendHandlerImpl(getHelper());
        if((name = groupHandler.getGroup(senderID).getGroupName()).isEmpty()){
            FriendDB friendDB = friendHandler.getFriend(senderID);
            name = String .format("%s %s", friendDB.getFirstName(), friendDB.getLastName());
        }
        return name;
    }

    protected Notification notificationCreator(String message,String senderID,Intent intent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        if(!senderID.isEmpty()) {
            mBuilder.setContentTitle(getSendersIdentity(senderID));
            mBuilder.setLargeIcon(Converters.getBitmapFromBytes(getSendersImage(senderID)));
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }else{
            mBuilder.setContentTitle("Group Invitation");
            mBuilder.setSmallIcon(R.mipmap.tickets);
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        }
        mBuilder.setContentText(message);
        mBuilder.setLights(Color.GREEN, 500, 500);

        long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
        mBuilder.setVibrate(pattern);
        mBuilder.setSound(Uri.parse("android.resource://"
                + getApplicationContext().getPackageName() + "/" + R.raw.run));
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                intent, 0);
        mBuilder.setContentIntent(pendingIntent);

        return mBuilder.build();
    }

    @Override
    public void onMessage(String topic, TempleBuffers.ChatMessage chatMessage) {
        MessageDB messageDB = messageHandler.saveMessage(chatMessage);
        Intent intent = new Intent(this,TempleActivity.class);
        intent.putExtra("SenderID",messageDB.getSenderID());
        notification = notificationCreator(messageDB.getMessage(), messageDB.getSenderID(),intent);
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notificationID, notification);

    }

    @Override
    public void onInvitation(TempleBuffers.Invitation invitation) {
        invitationHandler.addInvitation(invitation);
        Intent intent = new Intent(this,TempleActivity.class);
        notification = notificationCreator(invitation.getPersonalMessage(),"",intent);
        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notificationID, notification);
    }


    @Override
    public void onConnectionLost(String message) {

    }

    @Override
    public void onDeliveryComplete(String message) {

    }
}
