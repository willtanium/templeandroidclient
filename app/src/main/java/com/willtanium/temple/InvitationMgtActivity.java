package com.willtanium.temple;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.services.OnInvitationSentListener;
import com.willtanium.services.TempleServiceHandler;
import com.willtanium.storage.models.FriendDB;
import com.willtanium.storage.models.GroupDB;
import com.willtanium.views.displays.GroupSelectionView;
import com.willtanium.views.displays.InvitationsList;

import java.util.List;

/**
 * Created by william on 6/14/15.
 */
public class InvitationMgtActivity extends BaseActivity implements OnInvitationSentListener {
    private OnBackPressedListener backPressedListener;
    private GroupDB groupDB;
    private List<FriendDB> friendDBs;
    private TempleServiceHandler templeServiceHandler;




    public void setBackPressedListener(OnBackPressedListener backPressedListener) {
        this.backPressedListener = backPressedListener;
    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.invitation_main_activity);
        shiftToFragment(new InvitationsList());
        templeServiceHandler = new TempleServiceHandler(this);
        templeServiceHandler.setInvitationSentListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invitation_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.create_invitation) {
            shiftToFragment(new GroupSelectionView());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        backPressedListener.backButtonPressed(true);
    }

    public void setGroup(GroupDB groupDB) {

        this.groupDB = groupDB;
    }

    public void setSelectedFriends(List<FriendDB> choiceArray) {

        this.friendDBs = choiceArray;
    }

    public void sendInvitation(String personalMessage) {
        TempleBuffers.InvitationOut.Builder invitationOut = TempleBuffers.InvitationOut.newBuilder();
        invitationOut.setGroupId(groupDB.getGroupKey());
        for (FriendDB friendDB : friendDBs) {
            invitationOut.addContacts(friendDB.getFriendKey());
        }
        invitationOut.setPersonalMessage(personalMessage);
        templeServiceHandler.setInvitationSentListener(this);
        templeServiceHandler.sendInvitation(invitationOut.build());
    }

    @Override
    public void onInvitationSent(String response) {
        NiftyNotificationView.build(this, String.format(" %s ", response),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.upload) //You must call this method if you use ThumbSlider effect
                .show();
    }

    @Override
    public void onInvitationSendFailed(String message) {
        NiftyNotificationView.build(this, String.format(" %s ", message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.error)         //You must call this method if you use ThumbSlider effect
                .show();

    }


}
