package com.willtanium.temple;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.gitonway.lee.niftynotification.lib.Effects;
import com.gitonway.lee.niftynotification.lib.NiftyNotificationView;
import com.willtanium.protobuf.TempleBuffers;
import com.willtanium.services.OnGroupRegisteredListener;
import com.willtanium.services.TempleServiceHandler;
import com.willtanium.views.displays.GroupsList;
import com.willtanium.views.forms.GroupCreationFragment;

/**
 * Created by william on 6/14/15.
 */
public class GroupMgtActivity extends BaseActivity implements OnGroupRegisteredListener {
    private TempleServiceHandler templeServiceHandler;


    public void sendGroupDetails(TempleBuffers.GroupReg groupReg) {
        templeServiceHandler.registerGroup(groupReg);
        templeServiceHandler.setGroupRegisteredListener(this);

    }

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.group_main_activity);
        templeServiceHandler = new TempleServiceHandler(this);
        shiftToFragment(new GroupsList());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.create_group) {
            shiftToFragment(new GroupCreationFragment());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGroupRegistered(TempleBuffers.Group group) {
        groupHandler.saveGroup(group);
        shiftToFragment(new GroupsList());
        NiftyNotificationView.build(this, String.format("GROUP REGISTRATION for %s was successful",
                        group.getGroupName()),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.send_img)         //You must call this method if you use ThumbSlider effect
                .show();
    }

    @Override
    public void onGroupRegistrationFailed(String message) {
        NiftyNotificationView.build(this, String.format("Error", message),
                Effects.thumbSlider, R.id.mLyout)
                .setIcon(R.mipmap.upload)         //You must call this method if you use ThumbSlider effect
                .show();
    }
}
