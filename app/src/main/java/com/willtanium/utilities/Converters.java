package com.willtanium.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by william on 6/15/15.
 */
public class Converters {
    public static String dateToString(Date date){
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yy HH:mm");
        return DATE_FORMAT.format(date);
    }
    public static Bitmap getBitmapFromBytes(byte [] data){
        return BitmapFactory.decodeByteArray(data,0,data.length);
    }
}
