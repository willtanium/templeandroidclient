package com.willtanium.utilities;

import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by william on 6/15/15.
 */
public class FileHandler{

        private File file;
        private ObjectOutputStream outputStream;
        private DataOutputStream dataOutputStream;
        private DataInputStream dis = null;
        private ObjectInputStream inputStream;
        private  String filePath ;
        private String folderPath ="/temple";
        private byte[] fileData;
        public FileHandler(String storagePath) {
            this.filePath = storagePath;
            this.file = getFileInstance();

        }

        public static float getFileSize(String attachmentURI) {
            return 0;
        }

        public static String getFileName(String attachmentURI) {
            return null;
        }

        public void saveObject(Object object) {
            storeData(object);
        }

        public boolean isOpen() {
            return file.exists();
        }

        public Object getObject() {
            try {
                return readData();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }
        private boolean createDirectory()
        {
            boolean folderExists = false;
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() +folderPath);
            Log.i("FILE", String.format("Folder Path is %s", file.getAbsoluteFile()));
            if(!file.exists()) {

                file.mkdir();
                file.setWritable(true);
                folderExists = file.exists();
                Log.i("FILE","folder Created");

            }else{

                folderExists = file.exists();
                Log.i("FILE","folder exists");
            }
            return folderExists;
        }

        private File getFileInstance(){
            File ourFile = null;
            if(createDirectory()) {

                ourFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() +folderPath+"/"+ filePath);

                if(!ourFile.exists()) {
                    try {
                        ourFile.createNewFile();
                        Log.i("FILE","file created");
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.i("FILE","file creation failed");
                    }
                    ourFile.setWritable(true);
                }
            }
            return ourFile;
        }

        public Object readData() throws IOException, ClassNotFoundException {

            return deserialize(readDataBytes());
        }
        public byte [] readDataBytes(){
            try {
                Log.i("FILE LOGGER", file.getAbsolutePath());
                fileData = new byte[(int) file.length()];


                dis = new DataInputStream(new FileInputStream(file));
                dis.readFully(fileData);
                dis.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch(NullPointerException e){
                e.printStackTrace();
            }
            return fileData;
        }
        public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
            try {
                ByteArrayInputStream b = new ByteArrayInputStream(bytes);
                ObjectInputStream o = new ObjectInputStream(b);
                return o.readObject();
            }catch (NullPointerException e){
                return null;
            }
        }

        public boolean storeData(Object object){
            ByteArrayOutputStream b = new ByteArrayOutputStream();
            outputStream = null;
            try {
                outputStream = new ObjectOutputStream(b);
                outputStream.writeObject(object);
                return storeData(b.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        public boolean storeData(byte [] data){
            boolean isWritten = false;
            try {

                dataOutputStream = new DataOutputStream(new FileOutputStream(file));
                dataOutputStream.write(data);
                isWritten = true;
                dataOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }catch(NullPointerException e){
                e.printStackTrace();
            }
            return isWritten;
        }
    }


