package com.willtanium.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by william on 6/15/15.
 */
public class Loaders {

    private ContentResolver contentResolver;
    private Cursor cursor;
    private List<String> phoneNumbers = new ArrayList<String>();
    private TelephonyManager telephonyManager;
    private Context context;
    private String _ID = ContactsContract.Contacts._ID;
    private String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

    private Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
    private String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
    private String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

    public static Bitmap byteImageLoader(byte [] data, int width, int height){


        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data,0,data.length,bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/width, photoH/height);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        Bitmap bitmap = BitmapFactory.decodeByteArray(data,0,data.length,bmOptions);
        return bitmap;
    }


    public static List<String> getContacts(Context context) {
        Loaders loaders = new Loaders(context);
        return loaders.loadContacts();
    }

    private Loaders(Context context) {
        contentResolver = context.getContentResolver();
        cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        this.context = context;
    }


    private List<String> loadContacts() {
        // Loop Through Contacts in the Phone
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0) {
                    packPhoneNumber(contentResolver.query(PhoneCONTENT_URI,
                            null,
                            Phone_CONTACT_ID + "= ?",
                            new String[]{contact_id},
                            null));

                }

            }
        }
        return phoneNumbers;
    }

    private boolean packPhoneNumber(Cursor phoneCursor) {
        List<String> contacts = new LinkedList<>();
        while (phoneCursor.moveToNext()) {
            String number = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
            contacts.add(number);
        }
        phoneCursor.close();
        return phoneCursor.isClosed();
    }
}
